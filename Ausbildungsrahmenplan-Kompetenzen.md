<-- // Suchen: \s*[0-9]{0,2}(\s*X){1,}\s*(([0-9a-d\-–]{1,10}[,]?\s?){1,})$
// Ersetzen \t (Lernfeld $2) -->

# Kompetenzen:
## Allgemein zur Förderung der Kompetenzen
- zur persönlichen und strukturellen Reflexion,
- zum verantwortungsbewussten und eigenverantwortlichen Umgang mit zukunftsorientierten Technologien, digital vernetzten Medien sowie Daten- und Informationssystemen,
- in berufs- und fachsprachlichen Situationen adäquat zu handeln,
- zum lebensbegleitenden Lernen sowie zur beruflichen und individuellen Flexibilität zur Bewältigung der sich wandelnden Anforderungen in der Arbeitswelt und Gesellschaft,
- zur beruflichen Mobilität in Europa und einer globalisierten Welt




## Handlungskompetenz
Bereitschaft und Befähigung des Einzelnen, sich in beruflichen, gesellschaftlichen und privaten Situationen sachgerecht durchdacht sowie individuell und sozial verantwortlich zu verhalten.

### Fachkompetenz
Bereitschaft und Fähigkeit, auf der Grundlage fachlichen Wissens und Könnens Aufgaben und Probleme zielorientiert, sachgerecht, methodengeleitet und selbstständig zu lösen und das Ergebnis zu beurteilen.

### Selbstkompetenz ("Humankompetenz")
Bereitschaft und Fähigkeit, als individuelle Persönlichkeit die Entwicklungschancen, Anforderungen und Einschränkungen in Familie, Beruf und öffentlichem Leben zu klären, zu durchdenken und zu beurteilen, eigene Begabungen zu entfalten sowie Lebenspläne zu fassen und fortzuentwickeln. Sie umfasst Eigenschaften wie Selbstständigkeit, Kritikfähigkeit, Selbstvertrauen, Zuverlässigkeit, Verantwortungs- und Pflichtbewusstsein. Zu ihr gehören insbesondere auch die Entwicklung durchdachter Wertvorstellungen und die selbstbestimmte Bindung an Werte.

### Sozialkompetenz
Bereitschaft und Fähigkeit, soziale Beziehungen zu leben und zu gestalten, Zuwendungen und Spannungen zu erfassen und zu verstehen sowie sich mit anderen rational und verantwortungsbewusst auseinanderzusetzen und zu verständigen. Hierzu gehört insbesondere auch die Entwicklung sozialer Verantwortung und Solidarität.

## Methodenkompetenz
Bereitschaft und Fähigkeit zu zielgerichtetem, planmäßigem Vorgehen bei der Bearbeitung von Aufgaben und Problemen (zum Beispiel bei der Planung der Arbeitsschritte).

Kommunikative Kompetenzen
Bereitschaft und Fähigkeit, kommunikative Situationen zu verstehen und zu gestalten. Hierzu gehört es, eigene Absichten und Bedürfnisse sowie die der Partner wahrzunehmen, zu verstehen und darzustellen

##Lernkompetenz
Bereitschaft und Fähigkeit, Informationen über Sachverhalte und Zusammenhänge selbstständig und gemeinsam mit anderen zu verstehen, auszuwerten und in gedankliche Strukturen einzuordnen. Zur Lernkompetenz gehört insbesondere auch die Fähigkeit und Bereitschaft, im Beruf und über den Berufsbereich hinaus Lerntechniken und Lernstrategien zu entwickeln und diese für lebenslanges Lernen zu nutzen.


# Handlungsfelder

## Handlungsfeld 1: Unternehmens-/Betriebsmanagement, Arbeits-und Geschäftsprozesse (AGP)

- Unternehmensgründung (Lernfeld 1)
- Planung, Organisation, Steuerung und Kontrolle von betrieblichen Prozessen (Lernfeld 1, 3, 10a, 11b)
- Investitions-und Finanzierungsentscheidungen (Lernfeld 2, 10a)
- Controlling (Lernfeld 2)
- Personalmanagement (Lernfeld 12c)
- Marketing (Lernfeld 1)

## Handlungsfeld 2: Softwareentwicklung AGP
- Erfassung und Analyse einer Anforderungsbeschreibung nach Problemstellung (Lernfeld 2, 4, 12b)
- Auswahl und Anwendung der Werkzeuge (Lernfeld 7, 8, 12b)
- Modellierung des Softwaresystems (Lernfeld 9, 10a)
- Implementierung der Software (Lernfeld 3, 12b)
- Test der Software (Lernfeld 5, 11a, 10b, 10d)
- Erstellung von Dokumentationen (Lernfeld 5,8,9,11b)

## Handlungsfeld 3: Entwicklung von Hard-und Software-Systemlösungen AGP
- Erfassung und Analyse einer Kundenanforderung (Lernfeld 1, 2 10d)
- Machbarkeitsanalyse (Lernfeld 3, 10c)
- Planung und Erstellung eines Lösungskonzeptes (Lernfeld 7, 9, 10d)
- Zusammenstellung der Systemkomponenten (Lernfeld 7, 10d)
- Management von Projekten (Lernfeld 9, 12a, 12c)

## Handlungsfeld 4: Realisierung von Hard-und Software-Systemlösungen AGP
- Auswahl und Beschaffung von Systemkomponenten (Lernfeld 2, 7, 11a)
- Aufbau, Installation und Konfiguration von HW-und SW-Systemen (Lernfeld 7, 9, 10b)
- Test und Inbetriebnahme von HW-und SW-Systemen (Lernfeld 7, 11a)

## Handlungsfeld 5: Systembetreuung AGP
- Administration und Anpassung von HW-und SW-Systemen (Lernfeld 3, 12d)
- Überwachung, Wartung und Instandhaltung von HW-und SW-Systemen (Lernfeld 6, 11d)
- Erweiterung von HW-und SW-Systemen (Lernfeld 10b, 11b, 11c)

## Handlungsfeld 6: Kundenbetreuung AGP
- Abwicklung von Kundenaufträgen (Lernfeld 1, 7, 12c)
- Erbringung von Dienstleistungen (Lernfeld 2, 6, 11d)
- Schulung und Einweisung (Lernfeld 1, 11b, 12d)

## Handlungsfeld 7: Qualitätsmanagement AGP
- Festlegung und Anpassung von Qualitätsstandards (Lernfeld 6, 12a)
- Auswahl und Definition von Maßnahmen zur Qualitätssicherung (Lernfeld 8, 11b, 12b)
- Durchführung und Überprüfung von Qualitätssicherungsmaßnahmen (Lernfeld 5, 9, 11b)


# Liste der Entsprechungen zwischen Ausbildungsrahmenplan und Rahmenlehrplan

## Abschnitt A: fachrichtungsübergreifende berufsprofilgebende Fertigkeiten, Kenntnisse und Fähigkeiten

1. 	Planen, Vorbereiten und Durchführen von Arbeitsaufgaben in Abstimmung mit den kundenspezifischen Geschäfts- und Leistungsprozessen  (§ 4 Absatz 2 Nummer 1)
 a) Grundsätze und Methoden des Projektmanagements anwenden	 (Lernfeld 5, 12a-d)
 b) Auftragsunterlagen und Durchführbarkeit des Auftrags prüfen, insbesondere in Hinblick auf rechtliche, wirtschaftliche und terminliche Vorgaben, und den Auftrag mit den betrieblichen Prozessen und Möglichkeiten abstimmen	 (Lernfeld 2, 12a-d)
 c) Zeitplan und Reihenfolge der Arbeitsschritte für den eigenen Arbeitsbereich festlegen	 (Lernfeld 3, 12a-d)
 d) Termine planen und abstimmen sowie Terminüberwachung durchführen	 (Lernfeld 12a-d)
 e) Probleme analysieren und als Aufgabe definieren sowie Lösungsalternativen entwickeln und beurteilen	 (Lernfeld 2)
 f) Arbeits- und Organisationsmittel wirtschaftlich und ökologisch unter Berücksichtigung der vorhandenen Ressourcen und der Budgetvorgaben einsetzen	 (Lernfeld 2)
 g) Aufgaben im Team sowie mit internen und externen Kunden und Kundinnen planen und abstimmen	 (Lernfeld 2, 3)
 h) betriebswirtschaftlich relevante Daten erheben und bewerten und dabei Geschäfts- und Leistungsprozesse berücksichtigen,	 (Lernfeld 2, 3)
 i) eigene Vorgehensweise sowie die Aufgabendurchführung im Team reflektieren und bei der Verbesserung der Arbeitsprozesse mitwirken	 (Lernfeld 1 – 11 a-d)

2. 	Informieren und Beraten von Kunden und Kundinnen (§ 4 Absatz 2 Nummer 2)
  a) im Rahmen der Marktbeobachtung Preise, Leistungen und Konditionen von Wettbewerbern vergleichen	 (Lernfeld 1, 2)
	b) Bedarfe von Kunden und Kundinnen feststellen sowie Zielgruppen unterscheiden	 (Lernfeld 1, 2)
	c) Kunden unter Beachtung von Kommunikationsregeln informieren und Sachverhalte präsentieren und dabei deutsche und englische Fachbegriffe anwenden	 (Lernfeld 1, 2)
	d) Maßnahmen für Marketing und Vertrieb unterstützen  (kein Lernfeld, rein betrieblich)
	e) Informationsquellen auch in englischer Sprache aufgabenbezogen auswerten und für die Kundeninformation nutzen	 (Lernfeld 1, 2)
	f) Gespräche situationsgerecht führen und Kunden und Kundinnen unter Berücksichtigung der Kundeninteressen beraten	 (Lernfeld 3, 9, 12a-d)
	g) Kundenbeziehungen unter Beachtung rechtlicher Regelungen und betrieblicher Grundsätze gestalten	 (Lernfeld 6, 9, 12a-d)
	h) Daten und Sachverhalte interpretieren, multimedial aufbereiten und situationsgerecht unter Nutzung digitaler Werkzeuge und unter Berücksichtigung betrieblicher Vorgaben präsentieren	 (Lernfeld 6, 9, 12a-d)

3. 	Beurteilen marktgängiger IT-Systeme und kundenspezifischer Lösungen  (§ 4 Absatz 2  Nummer 3)
 a) marktgängige IT-Systeme für unterschiedliche Einsatzbereiche hinsichtlich Leistungsfähigkeit, Wirtschaftlichkeit und Barrierefreiheit beurteilen	 (Lernfeld 2, 3)
 b) Angebote zu IT-Komponenten, IT-Produkten und IT-Dienstleistungen einholen und bewerten sowie Spezifikationen und Konditionen vergleichen	 (Lernfeld 2, 3)
 c) technologische Entwicklungstrends von IT-Systemen feststellen sowie ihre wirtschaftlichen, sozialen und beruflichen Auswirkungen aufzeigen	 (Lernfeld 2, 3, 9)
 d) Veränderungen von Einsatzfeldern für IT-Systeme aufgrund technischer, wirtschaftlicher und gesellschaftlicher Entwicklungen feststellen	 (Lernfeld 2, 3, 9)

4. 	Entwickeln, Erstellen und Betreuen von IT-Lösungen  (§ 4 Absatz 2 Nummer 4)
 a) IT-Systeme zur Bearbeitung betrieblicher Fachaufgaben analysieren sowie unter Beachtung insbesondere von Lizenzmodellen, Urheberrechten und Barrierefreiheit konzeptionieren, konfigurieren, testen und dokumentieren	 (Lernfeld 3, 7, 9)
 b) Programmiersprachen, insbesondere prozedurale und objektorientierte Programmiersprachen, unterscheiden	 (Lernfeld 5)
 c) Systematisch Fehler erkennen, analysieren und beheben	 (Lernfeld 6)
 d) Algorithmen formulieren und Anwendungen in einer Programmiersprache erstellen	 (Lernfeld 5, 8, 10a-12a, 10c-12c)
 e) Datenbankmodelle unterscheiden, Daten organisieren und speichern sowie Abfragen erstellen	 (Lernfeld 5, 8, 10a-12a, 10c-12c)

5. 	Durchführen und Dokumentieren von qualitätssichernden Maßnahmen
(§ 4 Absatz 2 Nummer 5)
  a) betriebliche Qualitätssicherungssysteme im eigenen Arbeitsbereich anwenden und Qualitätssicherungsmaßnahmen projektbegleitend durchführen und dokumentieren	 (Lernfeld 3, 5, 6, 8, 11a, 12a-d)
	b) Ursachen von Qualitätsmängeln systematisch feststellen, beseitigen und dokumentieren	 (Lernfeld 3, 5, 6, 11a, 12a-d)
	c) im Rahmen eines Verbesserungsprozesses die Zielerreichung kontrollieren, insbesondere einen Soll-Ist-Vergleich durchführen	 (Lernfeld 1, 2, 6, 8)

6. 	Umsetzen, Integrieren und Prüfen von Maßnahmen zur IT-Sicherheit und zum Datenschutz (§ 4 Absatz 2 Nummer 6)
		a) betriebliche Vorgaben und rechtliche Regelungen zur IT-Sicherheit und zum Datenschutz einhalten	 (Lernfeld 4, 8, 9, 11b, 11d)
		b) Sicherheitsanforderungen von IT-Systemen analysieren und Maßnahmen zur IT-Sicherheit ableiten, abstimmen, umsetzen und evaluieren	 (Lernfeld 4, 8, 9, 11b, 11d)
		c) Bedrohungsszenarien erkennen und Schadenspotenziale unter Berücksichtigung wirtschaftlicher und technischer Kriterien einschätzen	 (Lernfeld 4, 8, 9, 11b, 11d)
		d) Kunden und Kundinnen im Hinblick auf Anforderungen an die IT-Sicherheit und an den Datenschutz beratenHihi		e) Wirksamkeit und Effizienz der umgesetzten Maßnahmen zur IT-Sicherheit und zum Datenschutz prüfen	 (Lernfeld 4, 8, 9, 11b, 11d)

7. Erbringen der Leistungen und  Auftragsabschluss  (§ 4 Absatz 2 Nummer 7)
    a) Leistungen nach betrieblichen und vertraglichen Vorgaben dokumentieren	 (Lernfeld 2, 6)
		b) Leistungserbringung unter Berücksichtigung der organisatorischen und terminlichen Vorgaben mit Kunden und Kundinnen abstimmen und kontrollieren	 (Lernfeld 6, 7, 8, 9, 11b, 11d, 12ad)
		c) Veränderungsprozesse begleiten und unterstützen	 (Lernfeld 2, 3, 7, 9, 11a, 11c)
		d) Kunden und Kundinnen in die Nutzung von Produkten und Dienstleistungen einweisen	 (Lernfeld 2, 12a-d)
		e) Leistungen und Dokumentationen an Kunden und Kundinnen übergeben sowie Abnahmeprotokolle anfertigen	 (Lernfeld 2, 6, 12a-d)
		f) Kosten für erbrachte Leistungen erfassen sowie im Zeitvergleich und im Soll-Ist-Vergleich bewerten	 (Lernfeld 2, 12a-d)

8. 	Betreiben von IT-Systemen (§ 4 Absatz 2 Nummer 8)
 a) Netzwerkkonzepte für unterschiedliche Anwendungsgebiete unterscheiden	 (Lernfeld 3, 9)
 b) Datenaustausch von vernetzten Systemen realisieren	 (Lernfeld 8)
 c) Verfügbarkeit und Ausfallwahrscheinlichkeiten analysieren und Lösungsvorschläge unterbreiten	 (Lernfeld 9, 11b, 11d)
 d) Maßnahmen zur präventiven Wartung und zur Störungsvermeidung einleiten und durchführen	 (Lernfeld 6, 7)
 e) Störungsmeldungen aufnehmen und analysieren sowie Maßnahmen zur  Störungsbeseitigung ergreifen	 (Lernfeld 6)
 f) Dokumentationen zielgruppengerecht und barrierefrei anfertigen, bereitstellen und pflegen, insbesondere technische Dokumentationen, System- sowie Benutzerdokumentationen	 (Lernfeld 4, 5, 8)

9. 	Inbetriebnehmen von Speicherlösungen  (§ 4 Absatz 2 Nummer 9)
 a) Sicherheitsmechanismen, insbesondere Zugriffsmöglichkeiten und rechte, festlegen und implementieren	 (Lernfeld 4, 9, 11b, 11d)
 b) Speicherlösungen, insbesondere Datenbanksysteme, integrieren	 (Lernfeld 5, 8)

10. Programmieren von Softwarelösungen  (§ 4 Absatz 2 Nummer 10)
 a) Programmspezifikationen festlegen, Datenmodelle und Strukturen aus fachlichen Anforderungen ableiten sowie Schnittstellen festlegen	 (Lernfeld 5, 10a-12a, 10c-12c, 10d-12d)
 b) Programmiersprachen auswählen und unterschiedliche Programmiersprachen anwenden	 (Lernfeld 5, 10a-12a, 10c-12c, 10d, 12d)
 c) Teilaufgaben von IT-Systemen automatisieren	 (Lernfeld 5, 10b)

# Abschnitt B: berufsprofilgebende Fertigkeiten, Kenntnisse und Fähigkeiten in der Fachrichtung Anwendungsentwicklung

1. Konzipieren und Umsetzen von kundenspezifischen Softwareanwendungen  (§ 4 Absatz 3 Nummer 1)
  a) Vorgehensmodelle und methoden sowie Entwicklungsumgebungen und bibliotheken auswählen und einsetzen	 (Lernfeld 5, 7,10a-12a)
  b) Analyse- und Designverfahren anwenden	 (Lernfeld 7, 8)
  c) Benutzerschnittstellen ergonomisch gestalten und an Kundenanforderungen anpassen	 (Lernfeld 7, 10a)
  d) Anwendungslösungen unter Berücksichtigung der bestehenden Systemarchitektur entwerfen und realisieren	 (Lernfeld 5, 8, 11a)
  e) Bestehende Anwendungslösungen anpassen	 (Lernfeld 5, 11a)
  f) Datenaustausch zwischen Systemen realisieren und unterschiedliche Datenquellen nutzen	 (Lernfeld 8)
  g) Komplexe Abfragen aus unterschiedlichen Datenquellen durchführen und Datenbestandsberichte erstellen	 (Lernfeld 8, 12a)

2. 	Sicherstellen der Qualität von Softwareanwendungen (§ 4 Absatz 3 Nummer 2)
  a) Sicherheitsaspekte bei der Entwicklung von Softwareanwendungen berücksichtigen	 (Lernfeld 8, 11a, 12a)
  b) Datenintegrität mithilfe von Werkzeugen sicherstellen	 (Lernfeld 5, 8, 12a)
	c) Modultests erstellen und durchführen	 (Lernfeld 5, 8, 10a, 11a)
	d) Werkzeuge zur Versionsverwaltung einsetzen	 (Lernfeld 5, 8, 10a-12a)
	e) Testkonzepte erstellen und Tests durchführen sowie Testergebnisse bewerten und dokumentieren	 (Lernfeld 5, 10a, 11a)
	f) Daten und Sachverhalte aus Tests multimedial aufbereiten und situationsgerecht unter Nutzung digitaler Werkzeuge und unter Beachtung der betrieblichen Vorgaben präsentieren	 (Lernfeld 5, 10a, 11a)

## Abschnitt C: berufsprofilgebende Fertigkeiten, Kenntnisse und Fähigkeiten in der Fachrichtung Systemintegration
1. 	Konzipieren und  Realisieren von ITSystemen (§ 4 Absatz 4 Nummer 1)
 a) Systemlösungen entsprechend den kundenspezifischen Anforderungen unter Berücksichtigung von Sicherheitsaspekten konzipieren	 (Lernfeld 10b)
 b) IT-Systeme auswählen, installieren und konfigurieren	 (Lernfeld 2, 9)
 c) Externe IT-Ressourcen bewerten, auswählen und in ein IT-System integrieren	 (Lernfeld 9)
 d) Kompatibilitätsprobleme von IT-Systemen und Systemkomponenten beurteilen und lösen	 (Lernfeld 3, 7)
 e) Testkonzepte erstellen sowie Tests durchführen und dokumentieren	 (Lernfeld 5, 7)
 f) Systemübergabe planen und mit den beteiligten Organisationseinheiten sowie Kunden und Kundinnen abstimmen und durchführen	 (Lernfeld 2, 12b)
 g) Datenübernahmen planen und durchführen	 (Lernfeld 8)

2. 	Installieren und Konfigurieren von Netzwerken (§ 4 Absatz 4 Nummer 2)
 a) Netzwerkprotokolle und -schnittstellen für unterschiedliche Anwendungsbereiche bewerten und auswählen	 (Lernfeld 10b)
 b) Netzwerkkomponenten auswählen, installieren und konfigurieren	 (Lernfeld 3, 10b)
 c) Systeme zur IT-Sicherheit in Netzwerken implementieren und dokumentieren	 (Lernfeld 11b)

3. 	Administrieren von IT-Systemen (§ 4 Absatz 4 Nummer 3)
 a) Richtlinien zur Nutzung von IT-Systemen erstellen und einführen	 (Lernfeld 10b, 11b)
 b) Lizenzrechte verwalten und die Einhaltung von Lizenzbestimmungen überwachen	 (Lernfeld 10b, 11b)
 c) Berechtigungskonzepte entwerfen, abstimmen und umsetzen	 (Lernfeld 10b, 11b)
 d) Systemaktualisierungen evaluieren und durchführen	 (Lernfeld 10b, 11b)
 e) Konzepte zur Datensicherung und -archivierung erstellen und umsetzen	 (Lernfeld 10b, 11b)
 f)	Konzepte zur Daten- und Systemwiederherstellung erstellen und umsetzen	 (Lernfeld 10b, 11b)
 g)	Systemauslastung über-wachen und Ressourcen verwalten	 (Lernfeld 10b, 11b)
 h) Systemverhalten überwachen, bewerten und Maßnahmen ergreifen	 (Lernfeld 10b, 11b)
 i) Benutzeranfragen aufnehmen, analysieren und bearbeiten	 (Lernfeld 10b, 11b)

## Abschnitt D: berufsprofilgebende Fertigkeiten, Kenntnisse und Fähigkeiten in der Fachrichtung Daten- und Prozessanalyse
1. 	Analysieren von Arbeits- und Geschäftsprozessen  (§ 4 Absatz 5 Nummer 1)
 a) betriebs- und produktionswirtschaftliche Geschäftsprozesse und ihr Zusammenwirken im Unternehmen analysieren	 (Lernfeld 1, 11c, 12c)
 b) Anforderungen in einer Prozessdarstellung abbilden	 (Lernfeld 11c, 12c)
 c) Werkzeuge der Prozessoptimierung vergleichen und vorschlagen	 (Lernfeld 11c, 12c)

2. 	Analysieren von Datenquellen und Bereitstellen von Daten  (§ 4 Absatz 5 Nummer 2)
 a) Daten aus heterogenen Datenquellen identifizieren und klassifizieren	 (Lernfeld 5, 8, 10c)
 b) Berechtigung zur Nutzung und zur Verknüpfung von Daten prüfen sowie entsprechende Maßnahmen ableiten	 (Lernfeld 4, 9)
 c) technische Voraussetzungen zur Übernahme von Daten sicherstellen und Daten bereitstellen	 (Lernfeld 8, 10c, 11c)

3. 	Nutzen der Daten zur Optimierung von Arbeits- und Geschäftsprozessen sowie zur Optimierung digitaler Geschäftsmodelle  (§ 4 Absatz 5  Nummer 3)
 a) Daten auf Qualität, insbesondere auf Plausibilität, Quantität, Redundanz, Vollständigkeit und Validität prüfen, Ergebnisse dokumentieren und bei Abweichungen vom Sollzustand Maßnahmen, insbesondere zur Verbesserung der Datenqualität, vorschlagen	 (Lernfeld 5, 8, 10c-12c)
 b) Auffindbarkeit, Zugänglichkeit, Interoperabilität, Wiederverwendbarkeit von Daten sicherstellen	 (Lernfeld 8, 12c)
 c) Analytische und statistische Verfahren anwenden	 (Lernfeld 11c)
 d) Programmiersprachen mit integrierten Auswertungsverfahren und Visualisierungswerkzeugen nutzen	 (Lernfeld 10c-12c)
 e) Ergebnisse der Analyse für unterschiedliche Zielgruppen aufbereiten	 (Lernfeld 8, 11c, 12c)
 f) Mathematische Vorhersagemodelle anwenden
 g) Werkzeuge zur Mustererkennung und zur Modellgenerierung nutzen	 (Lernfeld 10c)
 h) Analyseergebnisse zur Optimierung der betriebs- und produktionswirtschaftlichen Geschäftsprozesse nutzen	 (Lernfeld 11c)
 i) Kennzahlen ableiten und für ein Monitoringsystem vorschlagen	 (Lernfeld 7, 9, 12c)

 4. Umsetzen des Datenschutzes und der Schutzziele der Datensicherheit (§ 4 Absatz 5 Nummer 4)
 a) mit für Datenschutz zuständigen Personen und Einrichtungen kooperieren	 (Lernfeld 5)
 b) Benutzer- Zugriffs- und Datenhaltungs- sowie Datensicherungskonzepte erstellen und dabei die verschiedenen Datenklassifizierungen berücksichtigen	 (Lernfeld 4, 9)
 c) beim Umgang mit Daten und der Erstellung der Konzepte Datensparsamkeit und Datensorgfalt beachten	 (Lernfeld 4)
 d) Verfahren zur Datenverschlüsselung auswählen und nutzen	 (Lernfeld 8)

## Abschnitt E: berufsprofilgebende Fertigkeiten, Kenntnisse und Fähigkeiten in der Fachrichtung Digitale Vernetzung
1. 	Analysieren und Planen von Systemen zur Vernetzung von Prozessen und Produkten   (§ 4 Absatz 6 Nummer 1)
 a) das Zusammenwirken der Komponenten cyber-physischer Systeme erfassen und visualisieren	 (Lernfeld 10d)
 b) bestehende Vernetzung, eingesetzter Software und technischer Schnittstellen analysieren, insbesondere unter Berücksichtigung der bestehenden Netztopologie	 (Lernfeld 10d)
 c) Bei der Planung Aspekte der IT-Sicherheit und technische Rahmenbedingungen, insbesondere Netzwerkanforderungen, berücksichtigen	 (Lernfeld 11d)
 d) Netzwerkkomponenten auswählen, technische Unterlagen erstellen und Kosten kalkulieren	 (Lernfeld 11d)
 e) die Lösung zur Vernetzung und zu Änderungen am System kundenbezogen abstimmen	 (Lernfeld 11d)
 f) Daten auswerten und Vorschläge zur Optimierung der Interaktion von Systemen entwickeln	 (Lernfeld 11d)

2. 	Errichten, Ändern und Prüfen von vernetzten Systemen  (§ 4 Absatz 6 Nummer 2)
 a) Systemkomponenten und Netzwerkbetriebssysteme installieren, anpassen und konfigurieren	 (Lernfeld 9)
 b) Softwarelösungen zur Visualisierung und Optimierung von Prozessabläufen anwenden	 (Lernfeld 9)
 c) Programme erstellen und anpassen sowie Signal- und Datenübertragungseinrichtungen konfigurieren	 (Lernfeld 10d)
 d) Sicherheits- und Datensicherungssysteme berücksichtigen, Gefahrenpotenziale identifizieren und Zugangsberechtigungen festlegen	 (Lernfeld 11d)
 e) Testkonzepte erstellen, Tests durchführen, Fehler beseitigen sowie Ergebnisse und Änderungen dokumentieren	 (Lernfeld 11d)
 f) Systeme in Betrieb nehmen, Inbetriebnahmeprotokolle erstellen und Systeme übergeben	 (Lernfeld 11d)

3. 	Betreiben von vernetzten Systemen und Sicherstellung der Systemverfügbarkeit  (§ 4 Absatz 6 Nummer 3)
 a) Systemauslastung überwachen und Systemstatus dokumentieren	 (Lernfeld 11d)
 b) Systemdaten erfassen und im Hinblick auf Vorgabeparameter auswerten und Systemstörungen feststellen und beheben	 (Lernfeld 11d)
 c) Daten auswerten, um Wartungsintervalle und Prozessabläufe zu optimieren	 (Lernfeld 11d)
 d) System-, Diagnose- und Prozessdaten auswerten, Schwachstellen identifizieren und Maßnahmen ableiten	 (Lernfeld 11d)
 e) Angriffsszenarien in cyber-physischen Systemen unterscheiden und antizipieren	 (Lernfeld 11d)
 f) Anomalien in vernetzten Systemen feststellen und Schutzmaßnahmen einleiten	 (Lernfeld 11d)
 g) bereichsspezifische Sicherheitslösungen implementieren	 (Lernfeld 11d)
 h) Systemaktualisierungen vornehmen und Optimierungen vorschlagen	 (Lernfeld 11d, 12d)

## Abschnitt F: fachrichtungsübergreifende, integrativ zu vermittelnde Fertigkeiten, Kenntnisse und Fähigkeiten

1. 	Berufsbildung sowie Arbeits- und Tarifrecht  (§ 4 Absatz 7 Nummer 1)
 a) wesentliche Inhalte und Bestandteile des Ausbildungsvertrages darstellen, Rechte und Pflichten aus dem Ausbildungsvertrag feststellen und Aufgaben der Beteiligten im dualen System beschreiben 	während
 b) den betrieblichen Ausbildungsplan mit der Ausbildungsordnung vergleichen
 c) arbeits-, sozial- und mitbestimmungsrechtliche Vorschriften sowie für den Arbeitsbereich geltende Tarif- und Arbeitszeitregelungen beachten
 d) Positionen der eigenen Entgeltabrechnung erklären
 e) Chancen und Anforderungen des lebensbegleitenden Lernens für die berufliche und persönliche Entwicklung begründen und die eigenen Kompetenzen weiterentwickeln
 f) Lern- und Arbeitstechniken sowie Methoden des selbstgesteuerten Lernens anwenden und beruflich relevante Informationsquellen nutzen
 g) berufliche Aufstiegs- und Weiterentwicklungsmöglichkeiten darstellen

2. 	Aufbau und Organisation des Ausbildungsbetriebes (§ 4 Absatz 7  Nummer 2)
 a) die Rechtsform und den organisatorischen Aufbau des Ausbildungsbetriebes mit seinen Aufgaben und Zuständigkeiten sowie die Zusammenhänge zwischen den Geschäftsprozessen erläutern
 b) Beziehungen des Ausbildungsbetriebes und seiner Beschäftigten zu Wirtschaftsorganisationen, Berufsvertretungen und Gewerkschaften nennen
 c) Grundlagen, Aufgaben und Arbeitsweise der betriebsverfassungs- oder personalvertretungsrechtlichen Organe des Ausbildungsbetriebes beschreiben

3. 	Sicherheit und Gesundheitsschutz bei der Arbeit  (§ 4 Absatz 7 Nummer 3)
 a) Gefährdung von Sicherheit und Gesundheit am Arbeitsplatz feststellen und Maßnahmen zur Vermeidung der Gefährdung ergreifen
 b) berufsbezogene Arbeitsschutz- und Unfallverhütungsvorschriften anwenden
 c) Verhaltensweisen bei Unfällen beschreiben sowie erste Maßnahmen einleiten (betrieblich)
 d) Vorschriften des vorbeugenden Brandschutzes anwenden sowie Verhaltensweisen bei Bränden beschreiben und Maßnahmen zur Brandbekämpfung ergreifen (betrieblich)

 4. Umweltschutz 	(§ 4 Absatz 7 Nummer 4)  	Zur Vermeidung betriebsbedingter Umweltbelastungen im beruflichen Einwirkungsbereich beitragen, insbesondere
 a) mögliche Umweltbelastungen durch den Ausbildungsbetrieb und seinen Beitrag zum Umweltschutz an Beispielen erklären
 b) für den Ausbildungsbetrieb geltende Regelungen des Umweltschutzes anwenden
 c) Möglichkeiten der wirtschaftlichen und umweltschonenden Energie- und Materialverwendung nutzen
 d) Abfälle vermeiden sowie Stoffe und Materialien einer umweltschonenden Entsorgung zuführen

5. Vernetztes Zusammenarbeiten unter Nutzung digitaler Medien (§ 4 Absatz 7 Nummer 5)
	a) gegenseitige Wertschätzung unter Berücksichtigung gesellschaftlicher Vielfalt bei betrieblichen Abläufen praktizieren
	b) Strategien zum verantwortungsvollen Umgang mit digitalen Medien anwenden und im virtuellen Raum unter Wahrung der Persönlichkeitsrechte Dritter zusammenarbeiten
	c) insbesondere bei der Speicherung, Darstellung und Weitergabe digitaler Inhalte die Auswirkungen des eigenen Kommunikations- und Informationsverhaltens berücksichtigen
	d) bei der Beurteilung, Entwicklung, Umsetzung und Betreuung von IT-Lösungen ethische Aspekte reflektieren
