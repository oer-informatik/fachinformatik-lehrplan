# Lernfeld 01: Das Unternehmen und die eigene Rolle im Betrieb beschreiben (40WS ITD)

## Lehrplantext

**Die Schülerinnen und Schüler verfügen über die Kompetenz, ihr Unternehmen hinsichtlich  seiner  Wertschöpfungskette  zu präsentieren  und  ihre  eigene  Rolle  im  Betrieb  zu beschreiben.**

Die  Schülerinnen  und  Schüler **informieren** sich,  auch  anhand  des  Unternehmensleitbildes, über die ökonomischen, ökologischen und sozialen Zielsetzungen des Unternehmens.  

Sie **analysieren** die Marktstruktur in ihrer Branche und ordnen das Unternehmen als komplexes System mit seinen Markt- und Kundenbeziehungen ein. Sie beschreiben die Wertschöpfungskette und ihre eigene Rolle im Betrieb.

Dabei  erkunden  sie  die  Leistungsschwerpunkte  sowie  Besonderheiten  ihres  Unternehmens und  setzen  sich  mit  der  Organisationsstruktur  (_Aufbauorganisation_)  und  Rechtsform  auseinander.  Sie  informieren  sich  über  den  eigenen  Handlungs- und  Entscheidungsspielraum  im Unternehmen (Vollmachten) sowie über Fort-und Weiterbildungsmaßnahmen.

Sie  planen  und **erstellen**,  auch  im  Team,  adressatengerecht  multimediale  Darstellungen  zu ihrem Unternehmen.  

Die Schülerinnen und Schüler **präsentieren** ihre Ergebnisse.

Sie **überprüfen** kriteriengeleitet die Qualität ihres Handlungsproduktes und entwickeln gemeinsam Verbesserungsmöglichkeiten.

Sie **reflektieren** die eigene Rolle und das eigene Handeln im Betrieb.

## Zuordnung zur beruflichen Handlungsfeldern

### Fachbereichsspezifische Handlungsfelder und Arbeits- und Geschäftsprozesse

- HF1: Unternehmensgründung (Lernfeld 1)
- HF1: Planung, Organisation, Steuerung und Kontrolle von betrieblichen Prozessen (Lernfeld 1, 3, 10a, 11b)
- HF1: Marketing (Lernfeld 1)
- HF3: Erfassung und Analyse einer Kundenanforderung (Lernfeld 1, 2 10d)
- HF6: Abwicklung von Kundenaufträgen (Lernfeld 1, 7, 12c)
- HF6: Schulung und Einweisung (Lernfeld 1, 11b, 12d)

### Kompetenzzuordnung gemäß Rahmenlehrplan

#### Alle Fachinformatiker*innen

- 2 a) im Rahmen der Marktbeobachtung Preise, Leistungen und Konditionen von Wettbewerbern vergleichen	 (Lernfeld 1, 2)
- 2 b) Bedarfe von Kunden und Kundinnen feststellen sowie Zielgruppen unterscheiden	 (Lernfeld 1, 2)
- 2 c) Kunden unter Beachtung von Kommunikationsregeln informieren und Sachverhalte präsentieren und dabei deutsche und englische Fachbegriffe anwenden	 (Lernfeld 1, 2)
- 2 e) Informationsquellen auch in englischer Sprache aufgabenbezogen auswerten und für die Kundeninformation nutzen	 (Lernfeld 1, 2)
- 5 c) im Rahmen eines Verbesserungsprozesses die Zielerreichung kontrollieren, insbesondere einen Soll-Ist-Vergleich durchführen	 (Lernfeld 1, 2, 6, 8)
- 1 i) eigene Vorgehensweise sowie die Aufgabendurchführung im Team reflektieren und bei der Verbesserung der Arbeitsprozesse mitwirken	 (Lernfeld 1 – 11 a-d)

#### Nur IFP
- IFP 1 a) betriebs- und produktionswirtschaftliche Geschäftsprozesse und ihr Zusammenwirken im Unternehmen analysieren	 (Lernfeld 1, 11c, 12c)
