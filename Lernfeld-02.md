# Lernfeld 02: Arbeitsplätze nach Kundenwunsch ausstatten (80WS ITD)

## Lehrplantext

**Die Schülerinnen und Schüler verfügen über die Kompetenz, die Ausstattung eines Arbeitsplatzes nach Kundenwunsch zu dimensionieren, anzubieten, zu beschaffen und den Arbeitsplatz an die Kunden zu übergeben.**

Die Schülerinnen und Schüler nehmen den Kundenwunsch für die Ausstattung eines Arbeitsplatzes von internen und externen Kunden entgegen und **ermitteln** die sich daraus ergebenden Anforderungen  an  Soft- und  Hardware.  Aus  den  dokumentierten  Anforderungen  leiten  sie Auswahlkriterien für die Beschaffung ab. Sie berücksichtigen dabei die Einhaltung von Normen  und  Vorschriften  (_Zertifikate,  Kennzeichnung_)  für den  Betrieb  und  die  Sicherheit  von elektrischen Geräten und Komponenten.

Sie **vergleichen** die technischen Merkmale relevanter Produkte anhand von Datenblättern und Produktbeschreibungen zur Vorbereitung einer Auswahlentscheidung (_Nutzwertanalyse_). Dabei beachten sie insbesondere informationstechnische und energietechnische Kenngrößen sowie Aspekte der Ergonomie und der Nachhaltigkeit (Umweltschutz, Recycling). Sie wenden Recherchemethoden an und werten auch fremdsprachliche Quellen aus.

Sie ermitteln die Energieeffizienz unterschiedlicher Arbeitsplatzvarianten und dokumentieren diese.

Sie vergleichen mögliche Bezugsquellen (_quantitativer und qualitativer Angebotsvergleich_) und **bestimmen** den Lieferanten.

Auf  Basis  der  ausgewählten  Produkte  und  Lieferanten **erstellen** sie  mit  vorgegebenen  Zuschlagssätzen ein Angebot für die Kunden.

Sie  schließen  den  Kaufvertrag  ab  und  organisieren  den  Beschaffungsprozess  unter  Berück-sichtigung von Lieferzeiten. Sie nehmen die bestellten Komponenten in Empfang und dokumentieren dabei festgestellte Mängel.  

Sie bereiten die Übergabe der beschafften Produkte vor, integrieren IT-Komponenten, konfigurieren  diese  und  nehmen  sie  unter  Berücksichtigung  der  Arbeitssicherheit  in Betrieb.  Sie übergeben den Arbeitsplatz an die Kunden und erstellen ein Übergabeprotokoll.

Sie bewerten die Durchführung des Kundenauftrags und **reflektieren** ihr  Vorgehen.  Dabei berücksichtigen sie die Kundenzufriedenheit und formulieren Verbesserungsvorschläge.

### Kompetenzen gemäß Lernfeldbeschreibung:

#### Ich kann die Ausstattung eines  IT-System für einen Arbeitsplatz nach Kundenwunsch dimensionieren:
- ... Kundenwünsche erfassen
- ... Softwareanforderungen für einen Kundenarbeitsplatz  ermitteln und dokumentieren (LP LF2)
- ... Hardwareanforderungen für einen Kundenarbeitsplatz  ermitteln und dokumentieren
- ... informationstechnische und energietechnische Kenngrößen berücksichtigen
- ... aus Anforderungen Auswahlkriterien für die Beschaffung ableiten

#### Ich kann ein Angebot über Ausstattung eines IT-Systems für einen Arbeitsplatz erstellen:
- ... technischen Merkmale relevanter Produkte anhand von Datenblättern und Produktbeschreibungen vergleichen.
- ... Aspekte der Ergonomie und der Nachhaltigkeit (Umweltschutz, Recycling) berücksichtigen.
- ... Normen und Vorschriften für den Betrieb und die Sicherheit von elektrischen Geräten und Komponenten berücksichtigen. (Zertifikate, Kennzeichnung)
- ... die Energieeffizienz unterschiedlicher Arbeitsplatzvarianten ermitteln und dokumentieren.
- ... eine Auswahlentscheidung (Nutzwertanalyse) erstellen.
- ... mögliche Bezugsquellen für Komponenten vergleichen (quantitativer und qualitativer Angebotsvergleich) und Lieferanten bestimmen.
- ... auf Basis der ausgewählten Produkte und Lieferanten mit vorgegebenen Zuschlagssätzen ein Angebot für die Kunden erstellen.

#### Ich kann ein  IT-System für den Arbeitsplatz beschaffen:
- ... den Kaufvertrag abschließen und den Beschaffungsprozess unter Berücksichtigung von Lieferzeiten organisieren.
- ... die bestellten Komponenten in Empfang nehmen und die dabei festgestellte Mängel dokumentieren.


#### Ich kann das IT-System für den Arbeitsplatz an die Kunden übergeben:
- ... die Übergabe der beschafften Produkte vorbereiten,
- ... die IT-Komponenten ins System ingetrieren,
- ... das IT-System konfigurieren
- ... das IT-System unter Berücksichtigung der Arbeitssicherheit in Betrieb nehmen.
- ... das IT-System an die Kunden übergeben und ein Übergabeprotokoll erstellen.

#### Aufträge gemäß der vollständigen Handlung / PDCA durchführen:
- ... Recherchemethoden anwenden.
- ... fremdsprachliche Quellen auswerten
- ... die Durchführung des Kundenauftrags bewerten und mein Vorgehen bewerten
- ... Kundenzufriedenheit berücksichtigen und Verbesserungsvorschläge formulieren.

## Zuordnung zur beruflichen Handlungsfeldern

### Fachbereichsspezifische Handlungsfelder und Arbeits- und Geschäftsprozesse

- HF1: Investitions-und Finanzierungsentscheidungen (Lernfeld 2, 10a)
- HF1: Controlling (Lernfeld 2)
- HF2: Erfassung und Analyse einer Anforderungsbeschreibung nach Problemstellung (Lernfeld 2, 4, 12b)
- HF3: Erfassung und Analyse einer Kundenanforderung (Lernfeld 1, 2 10d)
- HF4: Auswahl und Beschaffung von Systemkomponenten (Lernfeld 2, 7, 11a)
- HF6: Erbringung von Dienstleistungen (Lernfeld 2, 6, 11d)

### Kompetenzzuordnung gemäß Rahmenlehrplan

#### Alle Fachinformatiker*innen

- 1 b) Auftragsunterlagen und Durchführbarkeit des Auftrags prüfen, insbesondere in Hinblick auf rechtliche, wirtschaftliche und terminliche Vorgaben, und den Auftrag mit den betrieblichen Prozessen und Möglichkeiten abstimmen	 (Lernfeld 2, 12a-d)
- 1 e) Probleme analysieren und als Aufgabe definieren sowie Lösungsalternativen entwickeln und beurteilen	 (Lernfeld 2)
- 1 f) Arbeits- und Organisationsmittel wirtschaftlich und ökologisch unter Berücksichtigung der vorhandenen Ressourcen und der Budgetvorgaben einsetzen	 (Lernfeld 2)
- 1 g) Aufgaben im Team sowie mit internen und externen Kunden und Kundinnen planen und abstimmen	 (Lernfeld 2, 3)
- 1 h) betriebswirtschaftlich relevante Daten erheben und bewerten und dabei Geschäfts- und Leistungsprozesse berücksichtigen,	 (Lernfeld 2, 3)
- 2 a) im Rahmen der Marktbeobachtung Preise, Leistungen und Konditionen von Wettbewerbern vergleichen	 (Lernfeld 1, 2)
- 2 b) Bedarfe von Kunden und Kundinnen feststellen sowie Zielgruppen unterscheiden	 (Lernfeld 1, 2)
- 2 c) Kunden unter Beachtung von Kommunikationsregeln informieren und Sachverhalte präsentieren und dabei deutsche und englische Fachbegriffe anwenden	 (Lernfeld 1, 2)
- 2 e) Informationsquellen auch in englischer Sprache aufgabenbezogen auswerten und für die Kundeninformation nutzen	 (Lernfeld 1, 2)
- 3 a) marktgängige IT-Systeme für unterschiedliche Einsatzbereiche hinsichtlich Leistungsfähigkeit, Wirtschaftlichkeit und Barrierefreiheit beurteilen	 (Lernfeld 2, 3)
- 3 b) Angebote zu IT-Komponenten, IT-Produkten und IT-Dienstleistungen einholen und bewerten sowie Spezifikationen und Konditionen vergleichen	 (Lernfeld 2, 3)
- 3 c) technologische Entwicklungstrends von IT-Systemen feststellen sowie ihre wirtschaftlichen, sozialen und beruflichen Auswirkungen aufzeigen	 (Lernfeld 2, 3, 9)
- 3 d) Veränderungen von Einsatzfeldern für IT-Systeme aufgrund technischer, wirtschaftlicher und gesellschaftlicher Entwicklungen feststellen	 (Lernfeld 2, 3, 9)
- 5 c) im Rahmen eines Verbesserungsprozesses die Zielerreichung kontrollieren, insbesondere einen Soll-Ist-Vergleich durchführen	 (Lernfeld 1, 2, 6, 8)
- 7 a) Leistungen nach betrieblichen und vertraglichen Vorgaben dokumentieren	 (Lernfeld 2, 6)
- 7 c) Veränderungsprozesse begleiten und unterstützen	 (Lernfeld 2, 3, 7, 9, 11a, 11c)
- 7 d) Kunden und Kundinnen in die Nutzung von Produkten und Dienstleistungen einweisen	 (Lernfeld 2, 12a-d)
- 7 e) Leistungen und Dokumentationen an Kunden und Kundinnen übergeben sowie Abnahmeprotokolle anfertigen	 (Lernfeld 2, 6, 12a-d)
- 7 f) Kosten für erbrachte Leistungen erfassen sowie im Zeitvergleich und im Soll-Ist-Vergleich bewerten	 (Lernfeld 2, 12a-d)
- 1 i) eigene Vorgehensweise sowie die Aufgabendurchführung im Team reflektieren und bei der Verbesserung der Arbeitsprozesse mitwirken	 (Lernfeld 1 – 11 a-d)

#### Nur IFS
- IFS 1 b) IT-Systeme auswählen, installieren und konfigurieren	 (Lernfeld 2, 9)
- IFS 1 f) Systemübergabe planen und mit den beteiligten Organisationseinheiten sowie Kunden und Kundinnen abstimmen und durchführen	 (Lernfeld 2, 12b)

#### ISE im Lehrplan NRW
"Im Ausbildungsberuf IT-Systemelektronikerinnen und IT-Systemelektroniker beginnt die För-derung von Kompetenzen zur Anbindung von IT-Systemen an die Stromversorgung bereits in Lernfeld 2. Hierbei bilden Maßnahmen zum Schutz gegen elektrische Gefährdung, Energiebe-darf und Leitungsdimensionierung einen Schwerpunkt. Die Förderung von Kompetenzen im Fachbereich Elektrotechnik wird in den weiteren Ausbildungsjahren insbesondere in den Lern-feldern 7, 10 und 11 fortgesetzt."
