# Lernfeld 03: Clients in Netzwerke einbinden (80WS VEP)

## Lehrplantext

**Die Schülerinnen und Schüler verfügen über die Kompetenz, eine Netzwerkinfrastruktur zu analysieren sowie Clients zu integrieren.**

Die  Schülerinnen  und  Schüler **erfassen** im  Kundengespräch  die  Anforderungen  an  die  In-tegration von Clients (_Soft- und Hardware_) in eine bestehende Netzwerkinfrastruktur und leiten Leistungskriterien ab.

Sie **informieren** sich über Strukturen und Komponenten des Netzwerkes und erfassen deren Eigenschaften  und  Standards.  Dazu  verwenden  sie  technische  Dokumente,  auch  in  fremder Sprache. Sie nutzen physische sowie logische  Netzwerkpläne und beachten betriebliche Sicherheitsvorgaben.

Sie **planen** die  Integration  in  die  bestehende  Netzwerkinfrastruktur  indem  sie  ein  anforderungsgerechtes  Konzept  auch  unter  ökologischen  und  wirtschaftlichen  Gesichtspunkten (_Energieeffizienz_) erstellen.

Sie **führen** auf der Basis der Leistungskriterien die Auswahl von Komponenten **durch**. Sie konfigurieren Clients und binden diese in das Netzwerk ein.

Sie **prüfen** systematisch die Funktion der konfigurierten Clients im Netzwerk und protokollieren das Ergebnis.

Sie **reflektieren** den  Arbeitsprozess  hinsichtlich  möglicher  Optimierungen  und  diskutieren das Ergebnis in Bezug auf Wirtschaftlichkeit und Ökologie.

## Zuordnung zur beruflichen Handlungsfeldern

### Fachbereichsspezifische Handlungsfelder und Arbeits- und Geschäftsprozesse

- HF1: Planung, Organisation, Steuerung und Kontrolle von betrieblichen Prozessen (Lernfeld 1, 3, 10a, 11b)
- HF2: Implementierung der Software (Lernfeld 3, 12b)
- HF3: Machbarkeitsanalyse (Lernfeld 3, 10c)
- HF5: Administration und Anpassung von HW-und SW-Systemen (Lernfeld 3, 12d)

### Kompetenzzuordnung gemäß Rahmenlehrplan

#### Alle Fachinformatiker*innen
- 1 c) Zeitplan und Reihenfolge der Arbeitsschritte für den eigenen Arbeitsbereich festlegen	 (Lernfeld 3, 12a-d)
- 1 g) Aufgaben im Team sowie mit internen und externen Kunden und Kundinnen planen und abstimmen	 (Lernfeld 2, 3)
- 1 h) betriebswirtschaftlich relevante Daten erheben und bewerten und dabei Geschäfts- und Leistungsprozesse berücksichtigen,	 (Lernfeld 2, 3)
- 1 i) eigene Vorgehensweise sowie die Aufgabendurchführung im Team reflektieren und bei der Verbesserung der Arbeitsprozesse mitwirken	 (Lernfeld 1 – 11 a-d)
- 2 f) Gespräche situationsgerecht führen und Kunden und Kundinnen unter Berücksichtigung der Kundeninteressen beraten	 (Lernfeld 3, 9, 12a-d)
- 3 a) marktgängige IT-Systeme für unterschiedliche Einsatzbereiche hinsichtlich Leistungsfähigkeit, Wirtschaftlichkeit und Barrierefreiheit beurteilen	 (Lernfeld 2, 3)
- 3 b) Angebote zu IT-Komponenten, IT-Produkten und IT-Dienstleistungen einholen und bewerten sowie Spezifikationen und Konditionen vergleichen	 (Lernfeld 2, 3)
- 3 c) technologische Entwicklungstrends von IT-Systemen feststellen sowie ihre wirtschaftlichen, sozialen und beruflichen Auswirkungen aufzeigen	 (Lernfeld 2, 3, 9)
- 3 d) Veränderungen von Einsatzfeldern für IT-Systeme aufgrund technischer, wirtschaftlicher und gesellschaftlicher Entwicklungen feststellen	 (Lernfeld 2, 3, 9)
- 4 a) IT-Systeme zur Bearbeitung betrieblicher Fachaufgaben analysieren sowie unter Beachtung insbesondere von Lizenzmodellen, Urheberrechten und Barrierefreiheit konzeptionieren, konfigurieren, testen und dokumentieren	 (Lernfeld 3, 7, 9)
- 5 a) betriebliche Qualitätssicherungssysteme im eigenen Arbeitsbereich anwenden und Qualitätssicherungsmaßnahmen projektbegleitend durchführen und dokumentieren	 (Lernfeld 3, 5, 6, 8, 11a, 12a-d)
- 5 b) Ursachen von Qualitätsmängeln systematisch feststellen, beseitigen und dokumentieren	 (Lernfeld 3, 5, 6, 11a, 12a-d)
- 7 c) Veränderungsprozesse begleiten und unterstützen	 (Lernfeld 2, 3, 7, 9, 11a, 11c)
- 8 a) Netzwerkkonzepte für unterschiedliche Anwendungsgebiete unterscheiden	 (Lernfeld 3, 9)

#### Nur IFS
- IFS 1 d) Kompatibilitätsprobleme von IT-Systemen und Systemkomponenten beurteilen und lösen	 (Lernfeld 3, 7)
- IFS 2 b) Netzwerkkomponenten auswählen, installieren und konfigurieren	 (Lernfeld 3, 10b)
