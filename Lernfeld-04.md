# Lernfeld 04: Schutzbedarfsanalyse im eigenen Arbeitsbereich durchführen (40WS VEP)


## Lehrplantext

**Die Schülerinnen und Schüler verfügen über die Kompetenz, mit Hilfe einer bestehenden  Sicherheitsleitlinie  eine  Schutzbedarfsanalyse  zur  Ermittlung  der  Informationssicherheit auf Grundschutzniveau in ihrem Arbeitsbereich durchzuführen.**

Die Schülerinnen und Schüler **informieren** sich über Informationssicherheit (Schutzziele) und rechtliche Regelungen sowie die Einhaltung von betrieblichen Vorgaben zur Bestimmung des Schutzniveaus für den eigenen Arbeitsbereich.

Sie **planen** eine Schutzbedarfsanalyse, indem sie gemäß der IT-Sicherheitsleitlinie des Unternehmens Schutzziele des Grundschutzes (_Vertraulichkeit, Integrität, Verfügbarkeit_) in ihrem Arbeitsbereich ermitteln und eine Klassifikation von Schadensszenarien vornehmen.

Sie **entscheiden** über  die  Gewichtung  möglicher  Bedrohungen  unter  Berücksichtigung  der Schadenszenarien.

Dazu **führen** sie eine Schutzbedarfsanalyse in ihrem Arbeitsbereich durch, nehmen Bedrohungsfaktoren auf und dokumentieren diese.

Die Schülerinnen und Schüler **bewerten** die Ergebnisse der Schutzbedarfsanalyse und gleichen diese mit der IT-Sicherheitsleitlinie des Unternehmens ab. Sie empfehlen Maßnahmen und setzen diese im eigenen Verantwortungsbereich um.

Sie **reflektieren** den Arbeitsablauf und übernehmen Verantwortung im IT-Sicherheitsprozess.

## Zuordnung zur beruflichen Handlungsfeldern

### Fachbereichsspezifische Handlungsfelder und Arbeits- und Geschäftsprozesse

- HF2: Erfassung und Analyse einer Anforderungsbeschreibung nach Problemstellung (Lernfeld 2, 4, 12b)

### Kompetenzzuordnung gemäß Rahmenlehrplan

#### Alle Fachinformatiker*innen
- 6 a) betriebliche Vorgaben und rechtliche Regelungen zur IT-Sicherheit und zum Datenschutz einhalten	 (Lernfeld 4, 8, 9, 11b, 11d)
- 6 b) Sicherheitsanforderungen von IT-Systemen analysieren und Maßnahmen zur IT-Sicherheit ableiten, abstimmen, umsetzen und evaluieren	 (Lernfeld 4, 8, 9, 11b, 11d)
- 6 c) Bedrohungsszenarien erkennen und Schadenspotenziale unter Berücksichtigung wirtschaftlicher und technischer Kriterien einschätzen	 (Lernfeld 4, 8, 9, 11b, 11d)
- 6 d) Kunden und Kundinnen im Hinblick auf Anforderungen an die IT-Sicherheit und an den Datenschutz beraten	 (Lernfeld 4, 8, 9, 11b, 11d)
- 6 e) Wirksamkeit und Effizienz der umgesetzten Maßnahmen zur IT-Sicherheit und zum Datenschutz prüfen	 (Lernfeld 4, 8, 9, 11b, 11d)
- 8 f) Dokumentationen zielgruppengerecht und barrierefrei anfertigen, bereitstellen und pflegen, insbesondere technische Dokumentationen, System- sowie Benutzerdokumentationen	 (Lernfeld 4, 5, 8)
- 9 a) Sicherheitsmechanismen, insbesondere Zugriffsmöglichkeiten und rechte, festlegen und implementieren	 (Lernfeld 4, 9, 11b, 11d)
- 1 i) eigene Vorgehensweise sowie die Aufgabendurchführung im Team reflektieren und bei der Verbesserung der Arbeitsprozesse mitwirken	 (Lernfeld 1 – 11 a-d)

#### Nur IFP:
- IFP 2 b) Berechtigung zur Nutzung und zur Verknüpfung von Daten prüfen sowie entsprechende Maßnahmen ableiten	 (Lernfeld 4, 9)
- IFP 4 b) Benutzer- Zugriffs- und Datenhaltungs- sowie Datensicherungskonzepte erstellen und dabei die verschiedenen Datenklassifizierungen berücksichtigen	 (Lernfeld 4, 9 )
- IFP 4 c) beim Umgang mit Daten und der Erstellung der Konzepte Datensparsamkeit und Datensorgfalt beachten	 (Lernfeld 4 )

#### Nur IFV:
- IFV 1 f) Daten auswerten und Vorschläge zur Optimierung der Interaktion von Systemen entwickeln	 (Lernfeld 11d )
