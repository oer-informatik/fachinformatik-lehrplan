# Lernfeld 05: Software zur Verwaltung von Daten anpassen (80WS SUD)

## Lehrplantext

**Die Schülerinnen und Schüler verfügen über die Kompetenz, Informationen mittels Daten abzubilden, diese Daten zu verwalten und dazu Software anzupassen.**

Die Schülerinnen und Schüler informieren sich innerhalb eines Projektes über die Abbildung von  Informationen  mittels  Daten.  Dabei **analysieren** sie Daten  hinsichtlich  Herkunft,  Art, Verfügbarkeit,  Datenschutz,  Datensicherheit  und  Speicheranforderung  und  berücksichtigen Datenformate und Speicherlösungen.

Sie **planen** die Anpassung einer Anwendung zur Verwaltung der Datenbestände und entwickeln Testfälle. Dabei **entscheiden** sie sich für ein Vorgehen.

Die  Schülerinnen  und  Schüler **implementieren** die  Anpassung  der  Anwendung,  auch  im Team und erstellen eine Softwaredokumentation.

Sie testen die Funktion der Anwendung und **beurteilen** deren Eignung zur Bewältigung der gestellten Anforderungen.

Sie **evaluierenden** Prozess der Softwareentwicklung.

### Kompetenzen gemäß Lernfeldbeschreibung aus dem Lehrplan:

#### Ich kann Informationen mittels Daten abbilden und diese analysieren:
... die Begriffe Daten und Informationen gegeneinander abgrenzen
... Daten hinsichtlich Herkunft, Art, Verfügbarkeit analysieren,
... Daten hinsichtlich Datenschutz analysieren,
... Daten hinsichtlich Datensicherheit analysieren,
... Daten hinsichtich Speicheranforderung analysieren,
... Daten hinsichtlich Datenformaten analysieren,
... Daten hinsichtlich Speicherlösungen analysieren


#### Ich kann Daten verwalten:
... Anpassungen von Anwendungen zur Verwaltung von Datenbeständen planen


#### Ich kann Software anpassen:
... Testfälle erstellen
... mich für ein geeignetes Vorgehen entscheiden
... Anpassungen im Team implementieren
... eine Softwaredokumentatoin erstellen
... die Funktionen testen und die Eignung der Software beurteilen
... den Prozess evaluieren

## Zuordnung zur beruflichen Handlungsfeldern

### Fachbereichsspezifische Handlungsfelder und Arbeits- und Geschäftsprozesse

- HF2: Test der Software (Lernfeld 5, 11a, 10b, 10d)
- HF2: Erstellung von Dokumentationen (Lernfeld 5,8,9,11b)
- HF7: Durchführung und Überprüfung von Qualitätssicherungsmaßnahmen (Lernfeld 5, 9, 11b)

### Kompetenzzuordnung gemäß Rahmenlehrplan

#### Alle Fachinformatiker*innen
- 1 a) Grundsätze und Methoden des Projektmanagements anwenden	 (Lernfeld 5, 12a-d)
- 4 b) Programmiersprachen, insbesondere prozedurale und objektorientierte Programmiersprachen, unterscheiden	 (Lernfeld 5)
- 4 d) Algorithmen formulieren und Anwendungen in einer Programmiersprache erstellen	 (Lernfeld 5, 8, 10a-12a, 10c-12c)
- 4 e) Datenbankmodelle unterscheiden, Daten organisieren und speichern sowie Abfragen erstellen	 (Lernfeld 5, 8, 10a-12a, 10c-12c)
- 5 a) betriebliche Qualitätssicherungssysteme im eigenen Arbeitsbereich anwenden und Qualitätssicherungsmaßnahmen projektbegleitend durchführen und dokumentieren	 (Lernfeld 3, 5, 6, 8, 11a, 12a-d)
- 5 b) Ursachen von Qualitätsmängeln systematisch feststellen, beseitigen und dokumentieren	 (Lernfeld 3, 5, 6, 11a, 12a-d)
- 8 f) Dokumentationen zielgruppengerecht und barrierefrei anfertigen, bereitstellen und pflegen, insbesondere technische Dokumentationen, System- sowie Benutzerdokumentationen	 (Lernfeld 4, 5, 8)
- 9 b) Speicherlösungen, insbesondere Datenbanksysteme, integrieren	 (Lernfeld 5, 8)
- 10 a) Programmspezifikationen festlegen, Datenmodelle und Strukturen aus fachlichen Anforderungen ableiten sowie Schnittstellen festlegen	 (Lernfeld 5, 10a-12a, 10c-12c, 10d-12d)
- 10 b) Programmiersprachen auswählen und unterschiedliche Programmiersprachen anwenden	 (Lernfeld 5, 10a-12a, 10c-12c, 10d, 12d)
- 10 c) Teilaufgaben von IT-Systemen automatisieren	 (Lernfeld 5, 10b)
- 1 i) eigene Vorgehensweise sowie die Aufgabendurchführung im Team reflektieren und bei der Verbesserung der Arbeitsprozesse mitwirken	 (Lernfeld 1 – 11 a-d)

#### Nur IFA:
- IFA 1 a) Vorgehensmodelle und methoden sowie Entwicklungsumgebungen und bibliotheken auswählen und einsetzen	 (Lernfeld 5, 7,10a-12a)
- IFA 1 d) Anwendungslösungen unter Berücksichtigung der bestehenden Systemarchitektur entwerfen und realisieren	 (Lernfeld 5, 8, 11a)
- IFA 1 e) Bestehende Anwendungslösungen anpassen	 (Lernfeld 5, 11a)
- IFA 2 b) Datenintegrität mithilfe von Werkzeugen sicherstellen	 (Lernfeld 5, 8, 12a)
- IFA 2 c) Modultests erstellen und durchführen	 (Lernfeld 5, 8, 10a, 11a)
- IFA 2 d) Werkzeuge zur Versionsverwaltung einsetzen	 (Lernfeld 5, 8, 10a-12a)
- IFA 2 e) Testkonzepte erstellen und Tests durchführen sowie Testergebnisse bewerten und dokumentieren	 (Lernfeld 5, 10a, 11a)
- IFA 2 f) Daten und Sachverhalte aus Tests multimedial aufbereiten und situationsgerecht unter Nutzung digitaler Werkzeuge und unter Beachtung der betrieblichen Vorgaben präsentieren	 (Lernfeld 5, 10a, 11a)

#### Nur IFS:
- IFS 1 e) Testkonzepte erstellen sowie Tests durchführen und dokumentieren	 (Lernfeld 5, 7)

#### Nur IFP:
- IFP 2 a) Daten aus heterogenen Datenquellen identifizieren und klassifizieren	 (Lernfeld 5, 8, 10c)
- IFP 3 a) Daten auf Qualität, insbesondere auf Plausibilität, Quantität, Redundanz, Vollständigkeit und Validität prüfen, Ergebnisse dokumentieren und bei Abweichungen vom Sollzustand Maßnahmen, insbesondere zur Verbesserung der Datenqualität, vorschlagen	 (Lernfeld 5, 8, 10c-12c)
- IFP 4 a) mit für Datenschutz zuständigen Personen und Einrichtungen kooperieren	 (Lernfeld 5 )
