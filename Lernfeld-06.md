# Lernfeld 06: Serviceanfragen bearbeiten (40WS)

## Lehrplantext

**Die Schülerinnen und Schüler verfügen über die Kompetenz, Serviceanfragen einzuordnen, Fehlerursachen zu ermitteln und zu beheben.**

Die  Schülerinnen  und  Schüler  nehmen  Serviceanfragen  entgegen  (_direkter  und  indirekter Kundenkontakt_). Sie **analysieren** Serviceanfragen und prüfen deren vertragliche Grundlage (_Service-Level-Agreement_). Sie ermitteln die Reaktionszeit und dokumentieren den Status der Anfragen im zugrundeliegenden Service-Management-System.

Durch  systematisches  Fragen **ordnen** die  Schülerinnen  und  Schüler  Serviceanfragen  unter Berücksichtigung des Support-Levels und fachlicher Standards **ein**.

Sie **ermitteln** Lösungsmöglichkeiten im Rahmen des Support-Levels. Auf dieser Basis bearbeitensie das Problem und dokumentieren den Bearbeitungsstatus. Sie kommunizieren mit den  Prozessbeteiligten  situationsgerecht,  auch  in  einer  Fremdsprache,  und  passen  sich  den unterschiedlichen  Kommunikationsanforderungen  an  (_Kommunikationsmodelle,  Deeskalationsstrategien_).

Sie **reflektieren** den  Bearbeitungsprozess der  Serviceanfragen  und  ihr  Verhalten  in  Gesprächssituationen. Die  Schülerinnen und Schüler diskutieren die Servicefälle und schlagen Maßnahmen zur Qualitätssteigerung vor.

## Zuordnung zur beruflichen Handlungsfeldern

### Fachbereichsspezifische Handlungsfelder und Arbeits- und Geschäftsprozesse

- HF5: Überwachung, Wartung und Instandhaltung von HW-und SW-Systemen (Lernfeld 6, 11d)
- HF6: Erbringung von Dienstleistungen (Lernfeld 2, 6, 11d)
- HF7: Festlegung und Anpassung von Qualitätsstandards (Lernfeld 6, 12a)

### Kompetenzzuordnung gemäß Rahmenlehrplan

#### Alle Fachinformatiker*innen
- 2 g) Kundenbeziehungen unter Beachtung rechtlicher Regelungen und betrieblicher Grundsätze gestalten	 (Lernfeld 6, 9, 12a-d)
- 2 h) Daten und Sachverhalte interpretieren, multimedial aufbereiten und situationsgerecht unter Nutzung digitaler Werkzeuge und unter Berücksichtigung betrieblicher Vorgaben präsentieren	 (Lernfeld 6, 9, 12a-d)
- 4 c) Systematisch Fehler erkennen, analysieren und beheben	 (Lernfeld 6)
- 5 a) betriebliche Qualitätssicherungssysteme im eigenen Arbeitsbereich anwenden und Qualitätssicherungsmaßnahmen projektbegleitend durchführen und dokumentieren	 (Lernfeld 3, 5, 6, 8, 11a, 12a-d)
- 5 b) Ursachen von Qualitätsmängeln systematisch feststellen, beseitigen und dokumentieren	 (Lernfeld 3, 5, 6, 11a, 12a-d)
- 5 c) im Rahmen eines Verbesserungsprozesses die Zielerreichung kontrollieren, insbesondere einen Soll-Ist-Vergleich durchführen	 (Lernfeld 1, 2, 6, 8)
- 7 a) Leistungen nach betrieblichen und vertraglichen Vorgaben dokumentieren	 (Lernfeld 2, 6)
- 7 b) Leistungserbringung unter Berücksichtigung der organisatorischen und terminlichen Vorgaben mit Kunden und Kundinnen abstimmen und kontrollieren	 (Lernfeld 6, 7, 8, 9, 11b, 11d, 12ad)
- 7 e) Leistungen und Dokumentationen an Kunden und Kundinnen übergeben sowie Abnahmeprotokolle anfertigen	 (Lernfeld 2, 6, 12a-d)
- 8 d) Maßnahmen zur präventiven Wartung und zur Störungsvermeidung einleiten und durchführen	 (Lernfeld 6, 7)
- 8 e) Störungsmeldungen aufnehmen und analysieren sowie Maßnahmen zur  Störungsbeseitigung ergreifen	 (Lernfeld 6)
- 1 i) eigene Vorgehensweise sowie die Aufgabendurchführung im Team reflektieren und bei der Verbesserung der Arbeitsprozesse mitwirken	 (Lernfeld 1 – 11 a-d)
