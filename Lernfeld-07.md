# Lernfeld 07: Cyber-physische Systeme ergänzen (80WS)

## Lehrplan

**Die Schülerinnen und Schüler verfügen über die Kompetenz, die physische Welt und IT-Systeme funktional zu einem cyber-physischen System zusammenzuführen.**

Die Schülerinnen und Schüler **analysieren** ein cyber-physisches System bezüglich eines Kundenauftrags zur Ergänzung und Inbetriebnahme weiterer Komponenten.

Sie **informieren** sich über den Datenfluss an der Schnittstelle zwischen physischer Welt und IT-System sowie über die Kommunikation in einem bestehenden Netzwerk. Sie verschaffen sich einen Überblick über die Energie-, Stoff- und Informationsflüsse aller am System beteiligten Geräte und Betriebsmittel.

Die Schülerinnen und Schüler **planen** die Umsetzung des Kundenwunsches, indem sie Kriterien für die Auswahl von Energieversorgung, Hardware und Software (_Bibliotheken, Protokolle_)  aufstellen.  Dazu  nutzen  sie  Unterlagen  der  technischen  Kommunikation  und  passen diese an.

Sie **führen** Komponenten mit dem cyber-physischen System funktional zusammen.

Sie **prüfen** systematisch  die  Funktion,  messen  physikalische  Betriebswerte,  validieren  den Energiebedarf und protokollieren die Ergebnisse.

Die Schülerinnen und Schüler **reflektierenden** Arbeitsprozess hinsichtlich möglicher Optimierungen und diskutieren das Ergebnis in Bezug auf Betriebssicherheit und Datensicherheit.

## Zuordnung zur beruflichen Handlungsfeldern

### Fachbereichsspezifische Handlungsfelder und Arbeits- und Geschäftsprozesse

- HF2: Auswahl und Anwendung der Werkzeuge (Lernfeld 7, 8, 12b)
- HF3: Planung und Erstellung eines Lösungskonzeptes (Lernfeld 7, 9, 10d)
- HF3: Zusammenstellung der Systemkomponenten (Lernfeld 7, 10d)
- HF4: Auswahl und Beschaffung von Systemkomponenten (Lernfeld 2, 7, 11a)
- HF4: Aufbau, Installation und Konfiguration von HW-und SW-Systemen (Lernfeld 7, 9, 10b)
- HF4: Test und Inbetriebnahme von HW-und SW-Systemen (Lernfeld 7, 11a)
- HF6: Abwicklung von Kundenaufträgen (Lernfeld 1, 7, 12c)

### Kompetenzzuordnung gemäß Rahmenlehrplan

#### Alle Fachinformatiker*innen
- 4 a) IT-Systeme zur Bearbeitung betrieblicher Fachaufgaben analysieren sowie unter Beachtung insbesondere von Lizenzmodellen, Urheberrechten und Barrierefreiheit konzeptionieren, konfigurieren, testen und dokumentieren	 (Lernfeld 3, 7, 9)
- 7 b) Leistungserbringung unter Berücksichtigung der organisatorischen und terminlichen Vorgaben mit Kunden und Kundinnen abstimmen und kontrollieren	 (Lernfeld 6, 7, 8, 9, 11b, 11d, 12ad)
- 7 c) Veränderungsprozesse begleiten und unterstützen	 (Lernfeld 2, 3, 7, 9, 11a, 11c)
- 8 d) Maßnahmen zur präventiven Wartung und zur Störungsvermeidung einleiten und durchführen	 (Lernfeld 6, 7)
- 1 i) eigene Vorgehensweise sowie die Aufgabendurchführung im Team reflektieren und bei der Verbesserung der Arbeitsprozesse mitwirken	 (Lernfeld 1 – 11 a-d)

#### Nur IFA:
- IFA 1 a) Vorgehensmodelle und methoden sowie Entwicklungsumgebungen und bibliotheken auswählen und einsetzen	 (Lernfeld 5, 7,10a-12a)
- IFA 1  b) Analyse- und Designverfahren anwenden	 (Lernfeld 7, 8)
- IFA 1  c) Benutzerschnittstellen ergonomisch gestalten und an Kundenanforderungen anpassen	 (Lernfeld 7, 10a)

#### Nur IFS
- IFS 1 d) Kompatibilitätsprobleme von IT-Systemen und Systemkomponenten beurteilen und lösen	 (Lernfeld 3, 7)
- IFS 1 e) Testkonzepte erstellen sowie Tests durchführen und dokumentieren	 (Lernfeld 5, 7)
- IFP 3 i) Kennzahlen ableiten und für ein Monitoringsystem vorschlagen	 (Lernfeld 7, 9, 12c )
