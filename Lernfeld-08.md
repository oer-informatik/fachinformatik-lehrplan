# Lernfeld 08: Daten systemübergreifend bereitstellen (80WS)

## Lehrplantext:

**Die Schülerinnen und Schüler besitzen die Kompetenz, Daten aus dezentralen Quellen zusammenzuführen, aufzubereiten und zur weiteren Nutzung zur Verfügung zu stellen.**

Die Schülerinnen und Schüler ermitteln für einen Kundenauftrag Datenquellen und **analysieren** diese hinsichtlich ihrer Struktur, rechtlicher Rahmenbedingungen, Zugriffsmöglichkeiten und -mechanismen.

Sie **wählen** die Datenquellen (heterogen)für den Kundenauftrag aus.

Sie **entwickeln** Konzepte zur Bereitstellung der gewählten Datenquellen für die weitere Ver-arbeitung unter Beachtung der Informationssicherheit.

Die Schülerinnen und Schüler **implementieren** arbeitsteilig, auch ortsunabhängig, ihr Konzept mit vorhandenen sowie dazu passenden Entwicklungswerkzeugen und Produkten.

Sie **übergeben** ihr Endprodukt mit Dokumentation zur Handhabung, auch in fremder Sprache, an die Kunden.

Sie **reflektieren** die  Eignung  der  eingesetzten  Entwicklungswerkzeuge  hinsichtlich  des  arbeitsteiligen Entwicklungsprozesses und die Qualität der Dokumentation.

## Zuordnung zur beruflichen Handlungsfeldern

### Fachbereichsspezifische Handlungsfelder und Arbeits- und Geschäftsprozesse

- HF2: Auswahl und Anwendung der Werkzeuge (Lernfeld 7, 8, 12b)
- HF2: Erstellung von Dokumentationen (Lernfeld 5,8,9,11b)
- HF7: Auswahl und Definition von Maßnahmen zur Qualitätssicherung (Lernfeld 8, 11b, 12b)

### Kompetenzzuordnung gemäß Rahmenlehrplan

#### Alle Fachinformatiker*innen
- 4 d) Algorithmen formulieren und Anwendungen in einer Programmiersprache erstellen	 (Lernfeld 5, 8, 10a-12a, 10c-12c)
- 4 e) Datenbankmodelle unterscheiden, Daten organisieren und speichern sowie Abfragen erstellen	 (Lernfeld 5, 8, 10a-12a, 10c-12c)
- 5 a) betriebliche Qualitätssicherungssysteme im eigenen Arbeitsbereich anwenden und Qualitätssicherungsmaßnahmen projektbegleitend durchführen und dokumentieren	 (Lernfeld 3, 5, 6, 8, 11a, 12a-d)
- 5 c) im Rahmen eines Verbesserungsprozesses die Zielerreichung kontrollieren, insbesondere einen Soll-Ist-Vergleich durchführen	 (Lernfeld 1, 2, 6, 8)
- 6 a) betriebliche Vorgaben und rechtliche Regelungen zur IT-Sicherheit und zum Datenschutz einhalten	 (Lernfeld 4, 8, 9, 11b, 11d)
- 6 b) Sicherheitsanforderungen von IT-Systemen analysieren und Maßnahmen zur IT-Sicherheit ableiten, abstimmen, umsetzen und evaluieren	 (Lernfeld 4, 8, 9, 11b, 11d)
- 6 c) Bedrohungsszenarien erkennen und Schadenspotenziale unter Berücksichtigung wirtschaftlicher und technischer Kriterien einschätzen	 (Lernfeld 4, 8, 9, 11b, 11d)
- 6 d) Kunden und Kundinnen im Hinblick auf Anforderungen an die IT-Sicherheit und an den Datenschutz beraten	 (Lernfeld 4, 8, 9, 11b, 11d)
- 6 e) Wirksamkeit und Effizienz der umgesetzten Maßnahmen zur IT-Sicherheit und zum Datenschutz prüfen	 (Lernfeld 4, 8, 9, 11b, 11d)
- 7 b) Leistungserbringung unter Berücksichtigung der organisatorischen und terminlichen Vorgaben mit Kunden und Kundinnen abstimmen und kontrollieren	 (Lernfeld 6, 7, 8, 9, 11b, 11d, 12ad)
- 8  b) Datenaustausch von vernetzten Systemen realisieren	 (Lernfeld 8)
- 8 f) Dokumentationen zielgruppengerecht und barrierefrei anfertigen, bereitstellen und pflegen, insbesondere technische Dokumentationen, System- sowie Benutzerdokumentationen	 (Lernfeld 4, 5, 8)
- 9 b) Speicherlösungen, insbesondere Datenbanksysteme, integrieren	 (Lernfeld 5, 8)
- 1 i) eigene Vorgehensweise sowie die Aufgabendurchführung im Team reflektieren und bei der Verbesserung der Arbeitsprozesse mitwirken	 (Lernfeld 1 – 11 a-d)

#### Nur IFA:
- IFA 1  b) Analyse- und Designverfahren anwenden	 (Lernfeld 7, 8)
- IFA 1 d) Anwendungslösungen unter Berücksichtigung der bestehenden Systemarchitektur entwerfen und realisieren	 (Lernfeld 5, 8, 11a)
- IFA 1 f) Datenaustausch zwischen Systemen realisieren und unterschiedliche Datenquellen nutzen	 (Lernfeld 8)
- IFA 1 g) Komplexe Abfragen aus unterschiedlichen Datenquellen durchführen und Datenbestandsberichte erstellen	 (Lernfeld 8, 12a)
- IFA 2 a) Sicherheitsaspekte bei der Entwicklung von Softwareanwendungen berücksichtigen	 (Lernfeld 8, 11a, 12a)
- IFA 2 b) Datenintegrität mithilfe von Werkzeugen sicherstellen	 (Lernfeld 5, 8, 12a)
- IFA 2 c) Modultests erstellen und durchführen	 (Lernfeld 5, 8, 10a, 11a)
- IFA 2 d) Werkzeuge zur Versionsverwaltung einsetzen	 (Lernfeld 5, 8, 10a-12a)

#### Nur IFS
- IFS 1 g) Datenübernahmen planen und durchführen	 (Lernfeld 8)

#### Nur IFP:
- IFP 2 a) Daten aus heterogenen Datenquellen identifizieren und klassifizieren	 (Lernfeld 5, 8, 10c)
- IFP 2 c) technische Voraussetzungen zur Übernahme von Daten sicherstellen und Daten bereitstellen	 (Lernfeld 8, 10c, 11c)
- IFP 3 a) Daten auf Qualität, insbesondere auf Plausibilität, Quantität, Redundanz, Vollständigkeit und Validität prüfen, Ergebnisse dokumentieren und bei Abweichungen vom Sollzustand Maßnahmen, insbesondere zur Verbesserung der Datenqualität, vorschlagen	 (Lernfeld 5, 8, 10c-12c )
- IFP 3 b) Auffindbarkeit, Zugänglichkeit, Interoperabilität, Wiederverwendbarkeit von Daten sicherstellen	 (Lernfeld 8, 12c )
- IFP 3 e) Ergebnisse der Analyse für unterschiedliche Zielgruppen aufbereiten	 (Lernfeld 8, 11c, 12c )
- IFP 4 d) Verfahren zur Datenverschlüsselung auswählen und nutzen	 (Lernfeld 8 )
