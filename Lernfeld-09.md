# Lernfeld 09: Netzwerke und Dienste bereitstellen (80WS)

## Lehrplantext

**Die Schülerinnen und Schüler verfügen über die Kompetenz, Netzwerke und Dienste zu planen, zu konfigurieren und zu erweitern.**

Die Schülerinnen und Schüler ermittelndie Anforderungen an ein Netzwerk in Kommunikation mit den Kunden. Sie **informieren** sich über  Eigenschaften,  Funktionen und Leistungsmerkmale der Netzwerkkomponenten und Dienste nach Kundenanforderung, auch unter Be-rücksichtigung  sicherheitsrelevanter  Merkmale.  Dabei  wenden  sie  Recherchemethoden  an und werten auch fremdsprachliche Quellen aus.

Sie **planen** die  erforderlichen  Dienste  und  dafür  notwendige  Netzwerke  sowie  deren  Infrastruktur unter Berücksichtigung interner und externer Ressourcen.

Dazu **vergleichen** sie Konzepte hinsichtlich ihrer Nachhaltigkeit sowie der technischen und wirtschaftlichen Eignung.

Sie **installieren** und konfigurieren Netzwerke sowie deren Infrastruktur und implementieren Dienste. Sie gewährleisten die Einhaltung von Standards, führen Funktionsprüfungen sowie Messungen durch und erstellen eine Dokumentation.

Die  Schülerinnen  und Schüler **beurteilen** die  Netzwerke  sowie  deren  Infrastruktur  und  die Dienste hinsichtlich der gestellten Anforderungen, Datensicherheit und Datenschutz.

Sie **reflektieren** ihre Lösung unter Berücksichtigung der Kundenzufriedenheit, Zukunftsfähigkeit und Vorgehensweise.


## Zuordnung zur beruflichen Handlungsfeldern

### Fachbereichsspezifische Handlungsfelder und Arbeits- und Geschäftsprozesse

- HF2: Modellierung des Softwaresystems (Lernfeld 9, 10a)
- HF2: Erstellung von Dokumentationen (Lernfeld 5,8,9,11b)
- HF3: Planung und Erstellung eines Lösungskonzeptes (7, 9, 10d)
- HF3: Management von Projekten (Lernfeld 9, 12a, 12c)
- HF4: Aufbau, Installation und Konfiguration von HW-und SW-Systemen (Lernfeld 7, 9, 10b)
- HF7: Durchführung und Überprüfung von Qualitätssicherungsmaßnahmen (Lernfeld 5, 9, 11b)

### Kompetenzzuordnung gemäß Rahmenlehrplan

#### Alle Fachinformatiker*innen
- 2 f) Gespräche situationsgerecht führen und Kunden und Kundinnen unter Berücksichtigung der Kundeninteressen beraten	 (Lernfeld 3, 9, 12a-d)
- 2 g) Kundenbeziehungen unter Beachtung rechtlicher Regelungen und betrieblicher Grundsätze gestalten	 (Lernfeld 6, 9, 12a-d)
- 2 h) Daten und Sachverhalte interpretieren, multimedial aufbereiten und situationsgerecht unter Nutzung digitaler Werkzeuge und unter Berücksichtigung betrieblicher Vorgaben präsentieren	 (Lernfeld 6, 9, 12a-d)
- 3 c) technologische Entwicklungstrends von IT-Systemen feststellen sowie ihre wirtschaftlichen, sozialen und beruflichen Auswirkungen aufzeigen	 (Lernfeld 2, 3, 9)
- 3 d) Veränderungen von Einsatzfeldern für IT-Systeme aufgrund technischer, wirtschaftlicher und gesellschaftlicher Entwicklungen feststellen	 (Lernfeld 2, 3, 9)
- 4 a) IT-Systeme zur Bearbeitung betrieblicher Fachaufgaben analysieren sowie unter Beachtung insbesondere von Lizenzmodellen, Urheberrechten und Barrierefreiheit konzeptionieren, konfigurieren, testen und dokumentieren	 (Lernfeld 3, 7, 9)
- 6 a) betriebliche Vorgaben und rechtliche Regelungen zur IT-Sicherheit und zum Datenschutz einhalten	 (Lernfeld 4, 8, 9, 11b, 11d)
- 6 b) Sicherheitsanforderungen von IT-Systemen analysieren und Maßnahmen zur IT-Sicherheit ableiten, abstimmen, umsetzen und evaluieren	 (Lernfeld 4, 8, 9, 11b, 11d)
- 6 c) Bedrohungsszenarien erkennen und Schadenspotenziale unter Berücksichtigung wirtschaftlicher und technischer Kriterien einschätzen	 (Lernfeld 4, 8, 9, 11b, 11d)
- 6 d) Kunden und Kundinnen im Hinblick auf Anforderungen an die IT-Sicherheit und an den Datenschutz beraten	 (Lernfeld 4, 8, 9, 11b, 11d)
- 6 e) Wirksamkeit und Effizienz der umgesetzten Maßnahmen zur IT-Sicherheit und zum Datenschutz prüfen	 (Lernfeld 4, 8, 9, 11b, 11d)
- 7 b) Leistungserbringung unter Berücksichtigung der organisatorischen und terminlichen Vorgaben mit Kunden und Kundinnen abstimmen und kontrollieren	 (Lernfeld 6, 7, 8, 9, 11b, 11d, 12ad)
- 7 c) Veränderungsprozesse begleiten und unterstützen	 (Lernfeld 2, 3, 7, 9, 11a, 11c)
- 8 a) Netzwerkkonzepte für unterschiedliche Anwendungsgebiete unterscheiden	 (Lernfeld 3, 9)
- 8 c) Verfügbarkeit und Ausfallwahrscheinlichkeiten analysieren und Lösungsvorschläge unterbreiten	 (Lernfeld 9, 11b, 11d)
- 9 a) Sicherheitsmechanismen, insbesondere Zugriffsmöglichkeiten und rechte, festlegen und implementieren	 (Lernfeld 4, 9, 11b, 11d)
- 1 i) eigene Vorgehensweise sowie die Aufgabendurchführung im Team reflektieren und bei der Verbesserung der Arbeitsprozesse mitwirken	 (Lernfeld 1 – 11 a-d)

#### Nur IFS
- IFS 1 b) IT-Systeme auswählen, installieren und konfigurieren	 (Lernfeld 2, 9)
- IFS 1 c) Externe IT-Ressourcen bewerten, auswählen und in ein IT-System integrieren	 (Lernfeld 9)

#### Nur IFP:
- IFP 2 b) Berechtigung zur Nutzung und zur Verknüpfung von Daten prüfen sowie entsprechende Maßnahmen ableiten	 (Lernfeld 4, 9)
- IFP 3 i) Kennzahlen ableiten und für ein Monitoringsystem vorschlagen	 (Lernfeld 7, 9, 12c )
- IFP 4 b) Benutzer- Zugriffs- und Datenhaltungs- sowie Datensicherungskonzepte erstellen und dabei die verschiedenen Datenklassifizierungen berücksichtigen	 (Lernfeld 4, 9 )

#### Nur IFV:
- IFV 2 a) Systemkomponenten und Netzwerkbetriebssysteme installieren, anpassen und konfigurieren	 (Lernfeld 9 )
- IFV 2 b) Softwarelösungen zur Visualisierung und Optimierung von Prozessabläufen anwenden	 (Lernfeld 9 )
