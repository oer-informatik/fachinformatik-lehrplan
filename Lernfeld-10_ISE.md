# Lernfeld 10 ISE: Cyber-physische Systeme entwickeln (80WS)

## Fachbereichsspezifische Handlungsfelder und Arbeits- und Geschäftsprozesse


## Kompetenzzuordnung gemäß Rahmenlehrplan

## Lehrplan NRW
"Im Ausbildungsberuf IT-Systemelektronikerinnen und IT-Systemelektroniker beginnt die För-derung von Kompetenzen zur Anbindung von IT-Systemen an die Stromversorgung bereits in Lernfeld 2. Hierbei bilden Maßnahmen zum Schutz gegen elektrische Gefährdung, Energiebe-darf und Leitungsdimensionierung einen Schwerpunkt. Die Förderung von Kompetenzen im Fachbereich Elektrotechnik wird in den weiteren Ausbildungsjahren insbesondere in den Lern-feldern 7, 10 und 11 fortgesetzt."
