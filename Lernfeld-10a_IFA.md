# Lernfeld 10a: Benutzerschnittstellen gestalten und entwickeln (80WS)

## Lehrplantext

**Die Schülerinnen und Schüler verfügen über die Kompetenz, Benutzeroberflächen für softwarebasierte Arbeitsabläufe und Geschäftsprozesse zu gestalten und zu entwickeln.**

Die Schülerinnen und Schüler **informieren** sich über die vorhandenen betrieblichen Abläufe und Geschäftsprozesse.

Sie **stellen** diese modellhaft **dar** und leiten Optimierungsmöglichkeiten ab. Sie gestalten und entwickeln mit agilen Methoden die Benutzeroberflächen für unterschiedliche Endgeräte und Betriebssysteme und stellen die vollständige Abbildung des  Informationsflusses unter Berücksichtigung der Prozessbeschreibung sicher.

Die Schülerinnen und Schüler **stellen** die Funktionalität der Softwarelösung her und nutzen hierzu bereits vorhandene Bibliotheken und Module.

Sie **überprüfen** das Produkt auf Datenschutzkonformität und Benutzerfreundlichkeit.

Die Schülerinnen und Schüler **testen** die funktionale Richtigkeit. Sie quantifizieren die Reduktion  der  Prozesskosten  des  digitalisierten,  optimierten  Geschäftsprozesses  und  stellen diese den Entwicklungskosten gegenüber.

## Zuordnung zur beruflichen Handlungsfeldern

### Fachbereichsspezifische Handlungsfelder und Arbeits- und Geschäftsprozesse

- HF1: Planung, Organisation, Steuerung und Kontrolle von betrieblichen Prozessen (Lernfeld 1, 3, 10a, 11b)
- HF1: Investitions-und Finanzierungsentscheidungen (Lernfeld 2, 10a)
- HF2: Modellierung des Softwaresystems (Lernfeld 9, 10a)

### Kompetenzzuordnung gemäß Rahmenlehrplan

- 1 i) eigene Vorgehensweise sowie die Aufgabendurchführung im Team reflektieren und bei der Verbesserung der Arbeitsprozesse mitwirken	 (Lernfeld 1 – 11 a-d)
- 4 d) Algorithmen formulieren und Anwendungen in einer Programmiersprache erstellen	 (Lernfeld 5, 8, 10a-12a, 10c-12c)
- 4 e) Datenbankmodelle unterscheiden, Daten organisieren und speichern sowie Abfragen erstellen	 (Lernfeld 5, 8, 10a-12a, 10c-12c)
- 5 a) betriebliche Qualitätssicherungssysteme im eigenen Arbeitsbereich anwenden und Qualitätssicherungsmaßnahmen projektbegleitend durchführen und dokumentieren	 (Lernfeld 3, 5, 6, 8, 11a, 12a-d)
- 5 b) Ursachen von Qualitätsmängeln systematisch feststellen, beseitigen und dokumentieren	 (Lernfeld 3, 5, 6, 11a, 12a-d)
- 10 a) Programmspezifikationen festlegen, Datenmodelle und Strukturen aus fachlichen Anforderungen ableiten sowie Schnittstellen festlegen	 (Lernfeld 5, 10a-12a, 10c-12c, 10d-12d)
- 10 b) Programmiersprachen auswählen und unterschiedliche Programmiersprachen anwenden	 (Lernfeld 5, 10a-12a, 10c-12c, 10d, 12d)
- IFA 1 a) Vorgehensmodelle und methoden sowie Entwicklungsumgebungen und bibliotheken auswählen und einsetzen	 (Lernfeld 5, 7,10a-12a)
- IFA 1  c) Benutzerschnittstellen ergonomisch gestalten und an Kundenanforderungen anpassen	 (Lernfeld 7, 10a)
- IFA 2 c) Modultests erstellen und durchführen	 (Lernfeld 5, 8, 10a, 11a)
- IFA 2 d) Werkzeuge zur Versionsverwaltung einsetzen	 (Lernfeld 5, 8, 10a-12a)
- IFA 2 e) Testkonzepte erstellen und Tests durchführen sowie Testergebnisse bewerten und dokumentieren	 (Lernfeld 5, 10a, 11a)
- IFA 2 f) Daten und Sachverhalte aus Tests multimedial aufbereiten und situationsgerecht unter Nutzung digitaler Werkzeuge und unter Beachtung der betrieblichen Vorgaben präsentieren	 (Lernfeld 5, 10a, 11a)
