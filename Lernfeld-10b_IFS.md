# Lernfeld 10b: Serverdienste bereitstellen und Administrationsaufgaben automatisieren (80WS)

## Lehrplantext

**Die Schülerinnen und Schüler verfügen über die Kompetenz, Serverdienste bereitzustellen, zu administrieren und zu überwachen.**

Die Schülerinnen und Schüler **informieren** sich über Serverdienste sowie Plattformen.

Sie **wählen** diese gemäß den Kundenanforderungen aus. Dabei berücksichtigen sie auch Verfügbarkeit, Skalierbarkeit, Administrierbarkeit, Wirtschaftlichkeit und Sicherheit.

Sie **planen** die Konfiguration der ausgewählten Dienste und erstellen Konzepte zur Einrichtung, Aktualisierung, Datensicherung und Überwachung.

Sie **implementieren** die Dienste unter Berücksichtigung betrieblicher Vorgaben und Lizenzierungen. Sie wenden Testverfahren an, überwachen die Dienste und empfehlen den Kunden Maßnahmen bei kritischen Zuständen. Sie dokumentieren ihre Ergebnisse.

Sie automatisieren Administrationsprozesse in Abhängigkeit kundenspezifischer Rahmenbedingungen, **testen** und optimieren die Automatisierung.

Die  Schülerinnen  und  Schüler **reflektieren** ihre  Lösung  und  beurteilen  sie  hinsichtlich  der Kundenanforderungen.

## Zuordnung zur beruflichen Handlungsfeldern

### Fachbereichsspezifische Handlungsfelder und Arbeits- und Geschäftsprozesse

- HF2: Test der Software (Lernfeld 5, 11a, 10b, 10d)
- HF4: Aufbau, Installation und Konfiguration von HW-und SW-Systemen (Lernfeld 7, 9, 10b)
- HF5: Erweiterung von HW-und SW-Systemen (Lernfeld 10b, 11b, 11c)

### Kompetenzzuordnung gemäß Rahmenlehrplan

- 1 i) eigene Vorgehensweise sowie die Aufgabendurchführung im Team reflektieren und bei der Verbesserung der Arbeitsprozesse mitwirken	 (Lernfeld 1 – 11 a-d)
- 10 c) Teilaufgaben von IT-Systemen automatisieren	 (Lernfeld 5, 10b)
- IFS 1 a) Systemlösungen entsprechend den kundenspezifischen Anforderungen unter Berücksichtigung von Sicherheitsaspekten konzipieren	 (Lernfeld 10b)
- IFS 2 a) Netzwerkprotokolle und -schnittstellen für unterschiedliche Anwendungsbereiche bewerten und auswählen	 (Lernfeld 10b)
- IFS 2 b) Netzwerkkomponenten auswählen, installieren und konfigurieren	 (Lernfeld 3, 10b)
- IFS 3 a) Richtlinien zur Nutzung von IT-Systemen erstellen und einführen	 (Lernfeld 10b, 11b)
- IFS 3 b) Lizenzrechte verwalten und die Einhaltung von Lizenzbestimmungen überwachen	 (Lernfeld 10b, 11b)
- IFS 3 c) Berechtigungskonzepte entwerfen, abstimmen und umsetzen	 (Lernfeld 10b, 11b)
- IFS 3 d) Systemaktualisierungen evaluieren und durchführen	 (Lernfeld 10b, 11b)
- IFS 3 e) Konzepte zur Datensicherung und -archivierung erstellen und umsetzen	 (Lernfeld 10b, 11b)
- IFS 3 f)	Konzepte zur Daten- und Systemwiederherstellung erstellen und umsetzen	 (Lernfeld 10b, 11b)
- IFS 3 g)	Systemauslastung über-wachen und Ressourcen verwalten	 (Lernfeld 10b, 11b)
- IFS 3 h) Systemverhalten überwachen, bewerten und Maßnahmen ergreifen	 (Lernfeld 10b, 11b)
- IFS 3 i) Benutzeranfragen aufnehmen, analysieren und bearbeiten	 (Lernfeld 10b, 11b)
