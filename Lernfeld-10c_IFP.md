# Lernfeld 10c: Werkzeuge des maschinellen Lernens einsetzen (80WS)

## Fachbereichsspezifische Handlungsfelder und Arbeits- und Geschäftsprozesse

- HF3: Machbarkeitsanalyse (Lernfeld 3, 10c)

## Kompetenzzuordnung gemäß Rahmenlehrplan

- 1 i) eigene Vorgehensweise sowie die Aufgabendurchführung im Team reflektieren und bei der Verbesserung der Arbeitsprozesse mitwirken	 (Lernfeld 1 – 11 a-d)
- 4 d) Algorithmen formulieren und Anwendungen in einer Programmiersprache erstellen	 (Lernfeld 5, 8, 10a-12a, 10c-12c)
- 4 e) Datenbankmodelle unterscheiden, Daten organisieren und speichern sowie Abfragen erstellen	 (Lernfeld 5, 8, 10a-12a, 10c-12c)
- 10 a) Programmspezifikationen festlegen, Datenmodelle und Strukturen aus fachlichen Anforderungen ableiten sowie Schnittstellen festlegen	 (Lernfeld 5, 10a-12a, 10c-12c, 10d-12d)
- 10 b) Programmiersprachen auswählen und unterschiedliche Programmiersprachen anwenden	 (Lernfeld 5, 10a-12a, 10c-12c, 10d, 12d)
- IFP 2 a) Daten aus heterogenen Datenquellen identifizieren und klassifizieren	 (Lernfeld 5, 8, 10c)
- IFP 2 c) technische Voraussetzungen zur Übernahme von Daten sicherstellen und Daten bereitstellen	 (Lernfeld 8, 10c, 11c)
- IFP 3 a) Daten auf Qualität, insbesondere auf Plausibilität, Quantität, Redundanz, Vollständigkeit und Validität prüfen, Ergebnisse dokumentieren und bei Abweichungen vom Sollzustand Maßnahmen, insbesondere zur Verbesserung der Datenqualität, vorschlagen	 (Lernfeld 5, 8, 10c-12c)
- IFP 3 d) Programmiersprachen mit integrierten Auswertungsverfahren und Visualisierungswerkzeugen nutzen	 (Lernfeld 10c-12c )
- IFP 3 g) Werkzeuge zur Mustererkennung und zur Modellgenerierung nutzen	 (Lernfeld 10c )
