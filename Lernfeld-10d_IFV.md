# Lernfeld 10d: Cyber-physische Systeme entwickeln (80WS)

## Fachbereichsspezifische Handlungsfelder und Arbeits- und Geschäftsprozesse

- HF2: Test der Software (Lernfeld 5, 11a, 10b, 10d)
- HF3: Erfassung und Analyse einer Kundenanforderung (Lernfeld 1, 2 10d)
- HF3: Planung und Erstellung eines Lösungskonzeptes (Lernfeld 7, 9, 10d)
- HF3: Zusammenstellung der Systemkomponenten (Lernfeld 7, 10d)


## Kompetenzzuordnung gemäß Rahmenlehrplan

- 1 i) eigene Vorgehensweise sowie die Aufgabendurchführung im Team reflektieren und bei der Verbesserung der Arbeitsprozesse mitwirken	 (Lernfeld 1 – 11 a-d)
- 10 a) Programmspezifikationen festlegen, Datenmodelle und Strukturen aus fachlichen Anforderungen ableiten sowie Schnittstellen festlegen	 (Lernfeld 5, 10a-12a, 10c-12c, 10d-12d)
- 10 b) Programmiersprachen auswählen und unterschiedliche Programmiersprachen anwenden	 (Lernfeld 5, 10a-12a, 10c-12c, 10d, 12d)
- IFV 1 a) das Zusammenwirken der Komponenten cyber-physischer Systeme erfassen und visualisieren	 (Lernfeld 10d )
- IFV 1 b) bestehende Vernetzung, eingesetzter Software und technischer Schnittstellen analysieren, insbesondere unter Berücksichtigung der bestehenden Netztopologie	 (Lernfeld 10d )
- IFV 2 c) Programme erstellen und anpassen sowie Signal- und Datenübertragungseinrichtungen konfigurieren	 (Lernfeld 10d )
