# Lernfeld 11a: Funktionalität in Anwendungen realisieren (80WS)

## Lehrplantext

**Die Schülerinnen und Schüler verfügen über die Kompetenz, modulare Komponenten zur informationstechnischen  Verarbeitung  von  Arbeitsabläufen  und  Geschäftsprozessen zu entwickeln und deren Qualität zu sichern.**

Die Schülerinnen und Schüler **leiten** aus den Informationsobjekten der vorgegebenen Prozessbeschreibungen der Kunden die dazu notwendigen Datenstrukturen und Funktionalitäten **ab**.

Sie **planen** modulare Softwarekomponenten und beschreiben deren Funktionsweise mit Diagrammen und Modellen.

Sie **wählen** eine  Methode  zur  Softwareentwicklung **aus**.  Dabei  beachten  sie,  dass  Planung, Realisierung und Tests iterativ in Abstimmung mit den Kunden erfolgen.

Die Schülerinnen und Schüler **realisieren**, auch im Team, die Softwarekomponenten und binden diese an Datenquellen an. Sie dokumentieren die Schnittstellen.

Sie **testen** die erforderliche Funktionalität, indem sie Testfälle formulieren und automatisierte Testverfahren anwenden.

Die Schülerinnen und Schüler **beurteilen** die Funktionalität anhand festgelegter Kriterien der Kunden und leiten Maßnahmen zur Überarbeitung der erstellten Module ein.

## Zuordnung zur beruflichen Handlungsfeldern

### Fachbereichsspezifische Handlungsfelder und Arbeits- und Geschäftsprozesse
- HF2: Test der Software (Lernfeld 5, 11a, 10b, 10d)
- HF4: Auswahl und Beschaffung von Systemkomponenten (Lernfeld 2, 7, 11a)
- HF4: Test und Inbetriebnahme von HW-und SW-Systemen (Lernfeld 7, 11a)

### Kompetenzzuordnung gemäß Rahmenlehrplan

- 1 i) eigene Vorgehensweise sowie die Aufgabendurchführung im Team reflektieren und bei der Verbesserung der Arbeitsprozesse mitwirken	 (Lernfeld 1 – 11 a-d)
- 4 d) Algorithmen formulieren und Anwendungen in einer Programmiersprache erstellen	 (Lernfeld 5, 8, 10a-12a, 10c-12c)
- 4 e) Datenbankmodelle unterscheiden, Daten organisieren und speichern sowie Abfragen erstellen	 (Lernfeld 5, 8, 10a-12a, 10c-12c)
- 7 c) Veränderungsprozesse begleiten und unterstützen	 (Lernfeld 2, 3, 7, 9, 11a, 11c)
- 10 a) Programmspezifikationen festlegen, Datenmodelle und Strukturen aus fachlichen Anforderungen ableiten sowie Schnittstellen festlegen	 (Lernfeld 5, 10a-12a, 10c-12c, 10d-12d)
- 10 b) Programmiersprachen auswählen und unterschiedliche Programmiersprachen anwenden	 (Lernfeld 5, 10a-12a, 10c-12c, 10d, 12d)
- IFA 1 a) Vorgehensmodelle und methoden sowie Entwicklungsumgebungen und bibliotheken auswählen und einsetzen	 (Lernfeld 5, 7,10a-12a)
- IFA 1 d) Anwendungslösungen unter Berücksichtigung der bestehenden Systemarchitektur entwerfen und realisieren	 (Lernfeld 5, 8, 11a)
- IFA 1 e) Bestehende Anwendungslösungen anpassen	 (Lernfeld 5, 11a)
- IFA 2 a) Sicherheitsaspekte bei der Entwicklung von Softwareanwendungen berücksichtigen	 (Lernfeld 8, 11a, 12a)
- IFA 2 c) Modultests erstellen und durchführen	 (Lernfeld 5, 8, 10a, 11a)
- IFA 2 d) Werkzeuge zur Versionsverwaltung einsetzen	 (Lernfeld 5, 8, 10a-12a)
- IFA 2 e) Testkonzepte erstellen und Tests durchführen sowie Testergebnisse bewerten und dokumentieren	 (Lernfeld 5, 10a, 11a)
- IFA 2 f) Daten und Sachverhalte aus Tests multimedial aufbereiten und situationsgerecht unter Nutzung digitaler Werkzeuge und unter Beachtung der betrieblichen Vorgaben präsentieren	 (Lernfeld 5, 10a, 11a)
