# Lernfeld 11b: Betrieb und Sicherheit vernetzter Systeme gewährleisten (80WS)

## Lehrplantext

Die Schülerinnen und Schüler verfügen über die Kompetenz, mit Hilfe einer Risikoanalyse den Schutzbedarf eines vernetzten Systems zu ermitteln und Schutzmaßnahmen zu planen, umzusetzen und zu dokumentieren.

Die Schülerinnen und Schüler bereiten sich auf ein Kundengespräch zur Identifizierung eines Schutzbedarfes  vor.  Hierzu  informieren  sie  sich  über  Informationssicherheit  in  vernetzten Systemen.

Sie ermitteln im Kundengespräch die Schutzziele, **analysieren** die Systeme hinsichtlich der Anforderungen an die Informationssicherheit und benennen Risiken.

Die Schülerinnen und Schüler **planen** unter Beachtung betrieblicher IT-Sicherheitsleitlinien und rechtlicher Regelungen die Vorkehrungen und Maßnahmen zur Minimierung der Wahrscheinlichkeit eines Schadenseintritts.

Sie **implementieren** die  Maßnahmen  unter  Berücksichtigung  technischer  und  organisatorischer Rahmenbedingungen.

Sie **prüfen** die Sicherheit des vernetzten Systems und **bewerten** das erreichte Sicherheitsniveau  in  Bezug  auf  die Kundenanforderungen,  eingesetzter  Maßnahmen  und  Wirtschaftlichkeit. Sie erstellen eine Dokumentation und informieren die Kunden über die Ergebnisse der Risikoanalyse.

Die Schülerinnen und Schüler **reflektieren** den Arbeitsprozess hinsichtlich möglicher Optimierungen und diskutieren das Ergebnis in Bezug auf den Begriff der relativen Sicherheit des vernetzten Systems.

## Zuordnung zur beruflichen Handlungsfeldern

### Fachbereichsspezifische Handlungsfelder und Arbeits- und Geschäftsprozesse

- HF1: Planung, Organisation, Steuerung und Kontrolle von betrieblichen Prozessen (Lernfeld 1, 3, 10a, 11b)
- HF2: Erstellung von Dokumentationen (Lernfeld 5,8,9,11b)
- HF5: Erweiterung von HW-und SW-Systemen (Lernfeld 10b, 11b, 11c)
- HF6: Schulung und Einweisung (Lernfeld 1, 11b, 12d)
- HF7: Auswahl und Definition von Maßnahmen zur Qualitätssicherung (Lernfeld 8, 11b, 12b)
- HF7: Durchführung und Überprüfung von Qualitätssicherungsmaßnahmen (Lernfeld 5, 9, 11b)

### Kompetenzzuordnung gemäß Rahmenlehrplan

- 1 i) eigene Vorgehensweise sowie die Aufgabendurchführung im Team reflektieren und bei der Verbesserung der Arbeitsprozesse mitwirken	 (Lernfeld 1 – 11 a-d)
- 6 a) betriebliche Vorgaben und rechtliche Regelungen zur IT-Sicherheit und zum Datenschutz einhalten	 (Lernfeld 4, 8, 9, 11b, 11d)
- 6 b) Sicherheitsanforderungen von IT-Systemen analysieren und Maßnahmen zur IT-Sicherheit ableiten, abstimmen, umsetzen und evaluieren	 (Lernfeld 4, 8, 9, 11b, 11d)
- 6 c) Bedrohungsszenarien erkennen und Schadenspotenziale unter Berücksichtigung wirtschaftlicher und technischer Kriterien einschätzen	 (Lernfeld 4, 8, 9, 11b, 11d)
- 6 d) Kunden und Kundinnen im Hinblick auf Anforderungen an die IT-Sicherheit und an den Datenschutz beraten	 (Lernfeld 4, 8, 9, 11b, 11d)
- 6 e) Wirksamkeit und Effizienz der umgesetzten Maßnahmen zur IT-Sicherheit und zum Datenschutz prüfen	 (Lernfeld 4, 8, 9, 11b, 11d)
- 7 b) Leistungserbringung unter Berücksichtigung der organisatorischen und terminlichen Vorgaben mit Kunden und Kundinnen abstimmen und kontrollieren	 (Lernfeld 6, 7, 8, 9, 11b, 11d, 12ad)
- 8 c) Verfügbarkeit und Ausfallwahrscheinlichkeiten analysieren und Lösungsvorschläge unterbreiten	 (Lernfeld 9, 11b, 11d)
- 9 a) Sicherheitsmechanismen, insbesondere Zugriffsmöglichkeiten und rechte, festlegen und implementieren	 (Lernfeld 4, 9, 11b, 11d)
- IFS 2 c) Systeme zur IT-Sicherheit in Netzwerken implementieren und dokumentieren	 (Lernfeld 11b)
- IFS 3 a) Richtlinien zur Nutzung von IT-Systemen erstellen und einführen	 (Lernfeld 10b, 11b)
- IFS 3 b) Lizenzrechte verwalten und die Einhaltung von Lizenzbestimmungen überwachen	 (Lernfeld 10b, 11b)
- IFS 3 c) Berechtigungskonzepte entwerfen, abstimmen und umsetzen	 (Lernfeld 10b, 11b)
- IFS 3 d) Systemaktualisierungen evaluieren und durchführen	 (Lernfeld 10b, 11b)
- IFS 3 e) Konzepte zur Datensicherung und -archivierung erstellen und umsetzen	 (Lernfeld 10b, 11b)
- IFS 3 f)	Konzepte zur Daten- und Systemwiederherstellung erstellen und umsetzen	 (Lernfeld 10b, 11b)
- IFS 3 g)	Systemauslastung über-wachen und Ressourcen verwalten	 (Lernfeld 10b, 11b)
- IFS 3 h) Systemverhalten überwachen, bewerten und Maßnahmen ergreifen	 (Lernfeld 10b, 11b)
- IFS 3 i) Benutzeranfragen aufnehmen, analysieren und bearbeiten	 (Lernfeld 10b, 11b)
