# Lernfeld 11c: Prozesse analysieren und gestalten (80WS)

## Fachbereichsspezifische Handlungsfelder und Arbeits- und Geschäftsprozesse

- HF5: Erweiterung von HW-und SW-Systemen (Lernfeld 10b, 11b, 11c)

## Kompetenzzuordnung gemäß Rahmenlehrplan

- 1 i) eigene Vorgehensweise sowie die Aufgabendurchführung im Team reflektieren und bei der Verbesserung der Arbeitsprozesse mitwirken	 (Lernfeld 1 – 11 a-d)
- 4 d) Algorithmen formulieren und Anwendungen in einer Programmiersprache erstellen	 (Lernfeld 5, 8, 10a-12a, 10c-12c)
- 4 e) Datenbankmodelle unterscheiden, Daten organisieren und speichern sowie Abfragen erstellen	 (Lernfeld 5, 8, 10a-12a, 10c-12c)
- 7 c) Veränderungsprozesse begleiten und unterstützen	 (Lernfeld 2, 3, 7, 9, 11a, 11c)
- 10 a) Programmspezifikationen festlegen, Datenmodelle und Strukturen aus fachlichen Anforderungen ableiten sowie Schnittstellen festlegen	 (Lernfeld 5, 10a-12a, 10c-12c, 10d-12d)
- 10 b) Programmiersprachen auswählen und unterschiedliche Programmiersprachen anwenden	 (Lernfeld 5, 10a-12a, 10c-12c, 10d, 12d)
- IFP 1 a) betriebs- und produktionswirtschaftliche Geschäftsprozesse und ihr Zusammenwirken im Unternehmen analysieren	 (Lernfeld 1, 11c, 12c)
- IFP 1 b) Anforderungen in einer Prozessdarstellung abbilden	 (Lernfeld 11c, 12c)
- IFP 1 c) Werkzeuge der Prozessoptimierung vergleichen und vorschlagen	 (Lernfeld 11c, 12c)
- IFP 2 c) technische Voraussetzungen zur Übernahme von Daten sicherstellen und Daten bereitstellen	 (Lernfeld 8, 10c, 11c)
- IFP 3 a) Daten auf Qualität, insbesondere auf Plausibilität, Quantität, Redundanz, Vollständigkeit und Validität prüfen, Ergebnisse dokumentieren und bei Abweichungen vom Sollzustand Maßnahmen, insbesondere zur Verbesserung der Datenqualität, vorschlagen	 (Lernfeld 5, 8, 10c-12c )
- IFP 3 c) Analytische und statistische Verfahren anwenden	 (Lernfeld 11c )
- IFP 3 d) Programmiersprachen mit integrierten Auswertungsverfahren und Visualisierungswerkzeugen nutzen	 (Lernfeld 10c-12c )
- IFP 3 e) Ergebnisse der Analyse für unterschiedliche Zielgruppen aufbereiten	 (Lernfeld 8, 11c, 12c )
- IFP 3 h) Analyseergebnisse zur Optimierung der betriebs- und produktionswirtschaftlichen Geschäftsprozesse nutzen	 (Lernfeld 11c )
