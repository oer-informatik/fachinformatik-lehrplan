# Lernfeld 11d: Betrieb und Sicherheit vernetzter Systeme gewährleisten

## Fachbereichsspezifische Handlungsfelder und Arbeits- und Geschäftsprozesse

- HF5: Überwachung, Wartung und Instandhaltung von HW-und SW-Systemen (Lernfeld 6, 11d)
- HF6: Erbringung von Dienstleistungen (Lernfeld 2, 6, 11d)

## Kompetenzzuordnung gemäß Rahmenlehrplan

- 1 i) eigene Vorgehensweise sowie die Aufgabendurchführung im Team reflektieren und bei der Verbesserung der Arbeitsprozesse mitwirken	 (Lernfeld 1 – 11 a-d)
- 6 a) betriebliche Vorgaben und rechtliche Regelungen zur IT-Sicherheit und zum Datenschutz einhalten	 (Lernfeld 4, 8, 9, 11b, 11d)
- 6 b) Sicherheitsanforderungen von IT-Systemen analysieren und Maßnahmen zur IT-Sicherheit ableiten, abstimmen, umsetzen und evaluieren	 (Lernfeld 4, 8, 9, 11b, 11d)
- 6 c) Bedrohungsszenarien erkennen und Schadenspotenziale unter Berücksichtigung wirtschaftlicher und technischer Kriterien einschätzen	 (Lernfeld 4, 8, 9, 11b, 11d)
- 6 d) Kunden und Kundinnen im Hinblick auf Anforderungen an die IT-Sicherheit und an den Datenschutz beraten	 (Lernfeld 4, 8, 9, 11b, 11d)
- 6 e) Wirksamkeit und Effizienz der umgesetzten Maßnahmen zur IT-Sicherheit und zum Datenschutz prüfen	 (Lernfeld 4, 8, 9, 11b, 11d)
- 7 b) Leistungserbringung unter Berücksichtigung der organisatorischen und terminlichen Vorgaben mit Kunden und Kundinnen abstimmen und kontrollieren	 (Lernfeld 6, 7, 8, 9, 11b, 11d, 12ad)
- 8 c) Verfügbarkeit und Ausfallwahrscheinlichkeiten analysieren und Lösungsvorschläge unterbreiten	 (Lernfeld 9, 11b, 11d)
- 9 a) Sicherheitsmechanismen, insbesondere Zugriffsmöglichkeiten und rechte, festlegen und implementieren	 (Lernfeld 4, 9, 11b, 11d)
- 10 a) Programmspezifikationen festlegen, Datenmodelle und Strukturen aus fachlichen Anforderungen ableiten sowie Schnittstellen festlegen	 (Lernfeld 5, 10a-12a, 10c-12c, 10d-12d)
- IFV 1 c) Bei der Planung Aspekte der IT-Sicherheit und technische Rahmenbedingungen, insbesondere Netzwerkanforderungen, berücksichtigen	 (Lernfeld 11d )
- IFV 1 d) Netzwerkkomponenten auswählen, technische Unterlagen erstellen und Kosten kalkulieren	 (Lernfeld 11d )
- IFV 1 e) die Lösung zur Vernetzung und zu Änderungen am System kundenbezogen abstimmen	 (Lernfeld 11d )
- IFV 1 f) Daten auswerten und Vorschläge zur Optimierung der Interaktion von Systemen entwickeln	 (Lernfeld 11d )
- IFV 2 d) Netzwerkkomponenten auswählen, technische Unterlagen erstellen und Kosten kalkulieren	 (Lernfeld 11d )
- IFV 2 e) die Lösung zur Vernetzung und zu Änderungen am System kundenbezogen abstimmen	 (Lernfeld 11d )
- IFV 2 f) Daten auswerten und Vorschläge zur Optimierung der Interaktion von Systemen entwickeln	 (Lernfeld 11d )
- IFV 3 a) Systemauslastung überwachen und Systemstatus dokumentieren	 (Lernfeld 11d )
- IFV 3 b) Systemdaten erfassen und im Hinblick auf Vorgabeparameter auswerten und Systemstörungen feststellen und beheben	 (Lernfeld 11d )
- IFV 3 c) Daten auswerten, um Wartungsintervalle und Prozessabläufe zu optimieren	 (Lernfeld 11d )
- IFV 3 d) System-, Diagnose- und Prozessdaten auswerten, Schwachstellen identifizieren und Maßnahmen ableiten	 (Lernfeld 11d )
- IFV 3 e) Angriffsszenarien in cyber-physischen Systemen unterscheiden und antizipieren	 (Lernfeld 11d )
- IFV 3 f) Anomalien in vernetzten Systemen feststellen und Schutzmaßnahmen einleiten	 (Lernfeld 11d )
- IFV 3 g) bereichsspezifische Sicherheitslösungen implementieren	 (Lernfeld 11d )
- IFV 3 h) Systemaktualisierungen vornehmen und Optimierungen vorschlagen	 (Lernfeld 11d, 12d )
