# Lernfeld 12a: Kundenspezifische Anwendungsentwicklung durchführen (120WS)

## Lehrplantext

**Die Schülerinnen und Schüler verfügen über die Kompetenz, einen Kundenauftrag zur Anwendungsentwicklung vollständig durchzuführen und zu bewerten.**

Die Schülerinnen und Schüler **führen** in Zusammenarbeit mit den Kunden eine Anforderungsanalyse **durch** und leiten daraus Projektziele, Anforderungen, gewünschte Ergebnisse, Schulungsbedarfe und Rahmenbedingungen ab.

Auf dieser Basis planen und kalkulieren sie ein Projekt mit den dazugehörigen personellen und technischen Ressourcen.

Die Schülerinnen und Schüler entwickeln Lösungsvarianten, vergleichen diese anhand festgelegter  Kriterien  sowie  unter  Berücksichtigung  von  Datenschutz  und  Datensicherheit.  Sie **wählen** mit den Kunden die beste Lösung **aus**. Für den vereinbarten Auftrag erstellen sie ein Dokument über die zu erbringenden Leistungen und ein Angebot.

Die  Schülerinnen  und  Schüler **implementieren** die  gewünschte  Lösung.  Dabei  nutzen  sie Maßnahmen  zur  Qualitätssicherung.  Sie  präsentierenden  Kundendas  Projektergebnis und führen eine Schulung durch. Sie übergeben den Kunden das Produkt sowie die Dokumentation.

Die Schülerinnen und Schüler **bewerten** das Projektergebnis auch hinsichtlich Zielerreichung, Wirtschaftlichkeit, Skalierbarkeit und Verlässlichkeit. Sie reflektieren die  Projektdurchführung  und  das  Projektergebnis  auch  unter  Berücksichtigung der kritisch-konstruktiven Kundenrückmeldungen.

## Zuordnung zur beruflichen Handlungsfeldern

### Fachbereichsspezifische Handlungsfelder und Arbeits- und Geschäftsprozesse

- HF3: Management von Projekten (Lernfeld 9, 12a, 12c)
- HF7: Festlegung und Anpassung von Qualitätsstandards (Lernfeld 6, 12a)

### Kompetenzzuordnung gemäß Rahmenlehrplan

#### Alle Fachinformatiker*innen
- 1 a) Grundsätze und Methoden des Projektmanagements anwenden	 (Lernfeld 5, 12a-d)
- 1 b) Auftragsunterlagen und Durchführbarkeit des Auftrags prüfen, insbesondere in Hinblick auf rechtliche, wirtschaftliche und terminliche Vorgaben, und den Auftrag mit den betrieblichen Prozessen und Möglichkeiten abstimmen	 (Lernfeld 2, 12a-d)
- 1 c) Zeitplan und Reihenfolge der Arbeitsschritte für den eigenen Arbeitsbereich festlegen	 (Lernfeld 3, 12a-d)
- 1 d) Termine planen und abstimmen sowie Terminüberwachung durchführen	 (Lernfeld 12a-d)
- 2 f) Gespräche situationsgerecht führen und Kunden und Kundinnen unter Berücksichtigung der Kundeninteressen beraten	 (Lernfeld 3, 9, 12a-d)
- 2 g) Kundenbeziehungen unter Beachtung rechtlicher Regelungen und betrieblicher Grundsätze gestalten	 (Lernfeld 6, 9, 12a-d)
- 2 h) Daten und Sachverhalte interpretieren, multimedial aufbereiten und situationsgerecht unter Nutzung digitaler Werkzeuge und unter Berücksichtigung betrieblicher Vorgaben präsentieren	 (Lernfeld 6, 9, 12a-d)
- 5 a) betriebliche Qualitätssicherungssysteme im eigenen Arbeitsbereich anwenden und Qualitätssicherungsmaßnahmen projektbegleitend durchführen und dokumentieren	 (Lernfeld 3, 5, 6, 8, 11a, 12a-d)
- 5 b) Ursachen von Qualitätsmängeln systematisch feststellen, beseitigen und dokumentieren	 (Lernfeld 3, 5, 6, 11a, 12a-d)
- 7 b) Leistungserbringung unter Berücksichtigung der organisatorischen und terminlichen Vorgaben mit Kunden und Kundinnen abstimmen und kontrollieren	 (Lernfeld 6, 7, 8, 9, 11b, 11d, 12ad)
- 7 d) Kunden und Kundinnen in die Nutzung von Produkten und Dienstleistungen einweisen	 (Lernfeld 2, 12a-d)
- 7 e) Leistungen und Dokumentationen an Kunden und Kundinnen übergeben sowie Abnahmeprotokolle anfertigen	 (Lernfeld 2, 6, 12a-d)
- 7 f) Kosten für erbrachte Leistungen erfassen sowie im Zeitvergleich und im Soll-Ist-Vergleich bewerten	 (Lernfeld 2, 12a-d)

#### Nur IFA:
- 4 d) Algorithmen formulieren und Anwendungen in einer Programmiersprache erstellen	 (Lernfeld 5, 8, 10a-12a, 10c-12c)
- 4 e) Datenbankmodelle unterscheiden, Daten organisieren und speichern sowie Abfragen erstellen	 (Lernfeld 5, 8, 10a-12a, 10c-12c)
- 10 a) Programmspezifikationen festlegen, Datenmodelle und Strukturen aus fachlichen Anforderungen ableiten sowie Schnittstellen festlegen	 (Lernfeld 5, 10a-12a, 10c-12c, 10d-12d)
- 10 b) Programmiersprachen auswählen und unterschiedliche Programmiersprachen anwenden	 (Lernfeld 5, 10a-12a, 10c-12c, 10d, 12d)
- IFA 1 a) Vorgehensmodelle und methoden sowie Entwicklungsumgebungen und bibliotheken auswählen und einsetzen	 (Lernfeld 5, 7,10a-12a)
- IFA 1 g) Komplexe Abfragen aus unterschiedlichen Datenquellen durchführen und Datenbestandsberichte erstellen	 (Lernfeld 8, 12a)
- IFA 2 a) Sicherheitsaspekte bei der Entwicklung von Softwareanwendungen berücksichtigen	 (Lernfeld 8, 11a, 12a)
- IFA 2 b) Datenintegrität mithilfe von Werkzeugen sicherstellen	 (Lernfeld 5, 8, 12a)
- IFA 2 d) Werkzeuge zur Versionsverwaltung einsetzen	 (Lernfeld 5, 8, 10a-12a)
