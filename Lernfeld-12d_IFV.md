# Lernfeld 12d: Kundenspezifisches cyber-physisches System optimieren (120WS)

## Fachbereichsspezifische Handlungsfelder und Arbeits- und Geschäftsprozesse

- HF5: Administration und Anpassung von HW-und SW-Systemen (Lernfeld 3, 12d)
- HF6: Schulung und Einweisung (Lernfeld 1, 11b, 12d)

## Kompetenzzuordnung gemäß Rahmenlehrplan

### Alle Fachinformatiker*innen
- 1 a) Grundsätze und Methoden des Projektmanagements anwenden	 (Lernfeld 5, 12a-d)
- 1 b) Auftragsunterlagen und Durchführbarkeit des Auftrags prüfen, insbesondere in Hinblick auf rechtliche, wirtschaftliche und terminliche Vorgaben, und den Auftrag mit den betrieblichen Prozessen und Möglichkeiten abstimmen	 (Lernfeld 2, 12a-d)
- 1 c) Zeitplan und Reihenfolge der Arbeitsschritte für den eigenen Arbeitsbereich festlegen	 (Lernfeld 3, 12a-d)
- 1 d) Termine planen und abstimmen sowie Terminüberwachung durchführen	 (Lernfeld 12a-d)
- 2 f) Gespräche situationsgerecht führen und Kunden und Kundinnen unter Berücksichtigung der Kundeninteressen beraten	 (Lernfeld 3, 9, 12a-d)
- 2 g) Kundenbeziehungen unter Beachtung rechtlicher Regelungen und betrieblicher Grundsätze gestalten	 (Lernfeld 6, 9, 12a-d)
- 2 h) Daten und Sachverhalte interpretieren, multimedial aufbereiten und situationsgerecht unter Nutzung digitaler Werkzeuge und unter Berücksichtigung betrieblicher Vorgaben präsentieren	 (Lernfeld 6, 9, 12a-d)
- 5 a) betriebliche Qualitätssicherungssysteme im eigenen Arbeitsbereich anwenden und Qualitätssicherungsmaßnahmen projektbegleitend durchführen und dokumentieren	 (Lernfeld 3, 5, 6, 8, 11a, 12a-d)
- 5 b) Ursachen von Qualitätsmängeln systematisch feststellen, beseitigen und dokumentieren	 (Lernfeld 3, 5, 6, 11a, 12a-d)
- 7 b) Leistungserbringung unter Berücksichtigung der organisatorischen und terminlichen Vorgaben mit Kunden und Kundinnen abstimmen und kontrollieren	 (Lernfeld 6, 7, 8, 9, 11b, 11d, 12ad)
- 7 d) Kunden und Kundinnen in die Nutzung von Produkten und Dienstleistungen einweisen	 (Lernfeld 2, 12a-d)
- 7 e) Leistungen und Dokumentationen an Kunden und Kundinnen übergeben sowie Abnahmeprotokolle anfertigen	 (Lernfeld 2, 6, 12a-d)
- 7 f) Kosten für erbrachte Leistungen erfassen sowie im Zeitvergleich und im Soll-Ist-Vergleich bewerten	 (Lernfeld 2, 12a-d)

### Nur IFV:
- 10 a) Programmspezifikationen festlegen, Datenmodelle und Strukturen aus fachlichen Anforderungen ableiten sowie Schnittstellen festlegen	 (Lernfeld 5, 10a-12a, 10c-12c, 10d-12d)
- 10 b) Programmiersprachen auswählen und unterschiedliche Programmiersprachen anwenden	 (Lernfeld 5, 10a-12a, 10c-12c, 10d, 12d)
- IFV 3 h) Systemaktualisierungen vornehmen und Optimierungen vorschlagen	 (Lernfeld 11d, 12d )
